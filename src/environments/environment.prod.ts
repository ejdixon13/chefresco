export const environment = {
  production: true,
  cloudinaryConfig: {
    cloudName: 'pietechsolutions',
    uploadPreset: 't0iey0lk'
  },
  firebaseConfig: {
    apiKey: 'AIzaSyDYDSdGAK9bBc-1-pddjm4tczvmLwzKss8',
    authDomain: 'cheffresco-f33e2.firebaseapp.com',
    databaseURL: 'https://cheffresco-f33e2.firebaseio.com',
    projectId: 'cheffresco-f33e2',
    storageBucket: 'cheffresco-f33e2.appspot.com',
    messagingSenderId: '608611749717',
    appId: '1:608611749717:web:8b1164c3faf7352c4a29dc',
    measurementId: 'G-58VZDKDZ33'
  },
  languages: [
    {
      'language': 'ENGLISH',
      'value': 'en'
    },
    {
      'language': 'FRENCH',
      'value': 'fr'
    },
    {
      'language': 'ARABIC',
      'value': 'ar'
    }
  ],
  payPalEnvironmentSandbox: 'AcgkbqWGamMa09V5xrhVC8bNP0ec9c37DEcT0rXuh7hqaZ6EyHdGyY4FCwQC-fII-s5p8FL0RL8rWPRB',
  stripePublishableKey: 'pk_live_kVkTsXUsM70DS711MtrkPqGO00BxV9z30N',
  stripeSecretKey: 'sk_test_GsisHcPqciYyG8arVfVe2amE', // Not really using
  azureApiEndpointSubmitOrder: 'https://chef-fresco.azurewebsites.net/api/StripeHttp?code=pKhb3rBkljvLxaGL4KMzSkjPcrphJi9XAxV5zCHMF6MBGlyHhjXKkg==',
  azureApiEndpointDuplicateUser: 'https://chef-fresco.azurewebsites.net/api/DuplicateUserCheck?code=I0ZnBVcfSZqIz8xnoqSRaYwCaGgdQbbC3jWSjHN/db4RA139Kk1/aQ==',
  apiEndpoint: 'https://restaurantrestapi.herokuapp.com/',
//   onesignalAppId: '230d3e93-0c29-49bd-ac82-ecea8612464e',
onesignalAppId: '9740a50f-587f-4853-821f-58252d998399',
//   googleProjectNumber: '714618018341'
googleProjectNumber: '314198464457'
};
