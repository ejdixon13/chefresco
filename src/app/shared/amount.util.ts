export function getTwoDecimalNum(num: number) {
    return parseFloat(num.toFixed(2));
}
