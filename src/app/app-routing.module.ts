import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'about-us', loadChildren: './pages/about-us/about-us.module#AboutUsPageModule' },
  { path: 'address', loadChildren: './pages/address/address.module#AddressPageModule' },
  { path: 'address-list', loadChildren: './pages/address-list/address-list.module#AddressListPageModule' },
  { path: 'booking-history', loadChildren: './pages/booking-history/booking-history.module#BookingHistoryPageModule' },
  { path: 'cart', loadChildren: './pages/cart/cart.module#CartPageModule' },
  { path: 'category', loadChildren: './pages/category/category.module#CategoryPageModule' },
  { path: 'chat', loadChildren: './pages/chat/chat.module#ChatPageModule' },
  { path: 'checkout', loadChildren: './pages/checkout/checkout.module#CheckoutPageModule' },
  { path: 'contact', loadChildren: './pages/contact/contact.module#ContactPageModule' },
  { path: 'favourite', loadChildren: './pages/favourite/favourite.module#FavouritePageModule' },
  { path: 'location', loadChildren: './pages/location/location.module#LocationPageModule' },
  { path: 'news', loadChildren: './pages/news/news.module#NewsPageModule' },
  { path: 'news-detail', loadChildren: './pages/news-detail/news-detail.module#NewsDetailPageModule' },
  { path: 'order-details', loadChildren: './pages/order-details/order-details.module#OrderDetailsPageModule' },
  { path: 'order-status', loadChildren: './pages/order-status/order-status.module#OrderStatusPageModule' },
  { path: 'orders', loadChildren: './pages/orders/orders.module#OrdersPageModule' },
  { path: 'offer', loadChildren: './pages/offer/offer.module#OfferPageModule' },
  { path: 'plan-selection', loadChildren: './pages/plan-selection/plan-selection.module#PlanSelectionPageModule' },
  {
    path: 'delivery-day-selection',
    loadChildren: './pages/delivery-day-selection/delivery-day-selection.module#DeliveryDaySelectionPageModule'
  },
  { path: 'product-list', loadChildren: './pages/product-list/product-list.module#ProductListPageModule' },
  { path: 'product-detail', loadChildren: './pages/product-detail/product-detail.module#ProductDetailPageModule' },
  { path: 'rating', loadChildren: './pages/rating/rating.module#RatingPageModule' },
  { path: 'registration', loadChildren: './pages/registration/registration.module#RegistrationPageModule' },
  { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule' },
  { path: 'sign-up', loadChildren: './pages/sign-up/sign-up.module#SignUpPageModule' },
  { path: 'book-table', loadChildren: './pages/table-booking/table-booking.module#TableBookingPageModule' },
  { path: 'thankyou', loadChildren: './pages/thankyou/thankyou.module#ThankyouPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
