import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ThankyouPage } from './thankyou.page';
import { TranslaterCustomModule } from './../../../app/translate.module';


const routes: Routes = [
  {
    path: '',
    component: ThankyouPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslaterCustomModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ThankyouPage]
})
export class ThankyouPageModule { }
