import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})

export class RegistrationService {

  constructor(private afAuth: AngularFireAuth, private afFS: AngularFirestore) { }

  createUser(user: any) {
    return this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
  }

  saveUserDetail(user: any, uid: any) {
    return this.afFS.doc('users/' + uid).set({
      name: user.name,
      email: user.email,
      mobileNo: user.phone,
      role: 'User'
    });
  }

  getDeliveryAreaForZipcode(zip: string) {
    return this.afFS.collection('delivery-options', ref => ref.where('zipCodeList', 'array-contains', zip));
  }

}




