import { Service } from './../../services/app.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, SelectControlValueAccessor, AbstractControl } from '@angular/forms';
// import { Facebook } from '@ionic-native/facebook';
// import { GooglePlus } from '@ionic-native/google-plus';
// import { TwitterConnect } from '@ionic-native/twitter-connect';
import { RegistrationService } from './registration.service';
import { SocketService } from 'src/app/services/socket.service';
import { NavController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { OrderFlowService, Order, Address } from 'src/app/services/order-flow.service';
import { take, tap, switchMap, catchError, map } from 'rxjs/operators';
import { throwError, Observable, of } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { APP_IS_ON_DEVICE } from 'src/app/shared/constant';
import { get } from 'lodash';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
  providers: [RegistrationService]
})
export class RegistrationPage implements OnInit {
  user: FormGroup;
  isLoggedIn: boolean;

  constructor(
    private navCtrl: NavController,
    public fb: FormBuilder,
    private service: Service,
    // public facebook: Facebook,
    // public googlePlus: GooglePlus,
    // public twitter: TwitterConnect,
    private afAuth: AngularFireAuth,
    private afFS: AngularFirestore,
    public orderFlowService: OrderFlowService,
    private registrationService: RegistrationService,
    private userService: UserService

  ) { }

  ngOnInit() {
    if (localStorage.getItem('token')) {
      localStorage.removeItem('token');
    }
    this.isLoggedIn = this.service.isLoggedIn();
    const emailInput = this.isLoggedIn ? ['']
      : ['', [
        Validators.required,
        Validators.email
      ]];
    this.user = this.fb.group({
      email: emailInput,
      zip: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  async onRegister() {
    // const loader = await this.service.showLoading();
    // this.registrationService.createUser(this.user.value).then((success: any) => {
    //   this.registrationService.saveUserDetail(this.user.value, success.user.uid).then((res: any) => {
    //     console.log(res)
    //     localStorage.setItem('uid', success.user.uid);
    //     loader.dismiss();
    //     this.service.showToaster('Registration successful!');
    //     this.navCtrl.navigateRoot('/home');
    //   }).catch(error => {
    //     loader.dismiss();
    //     this.service.showToaster('Registration Failed');
    //   })
    // }).catch(error => {
    //   loader.dismiss();
    //   this.service.showToaster('Registration Failed');
    // })
  }

  async continue() {
    const loader = await this.service.showLoading();
    let obs$;
    if (this.isLoggedIn) {
      obs$ = this.performDeliveryAreaCheck(this.user.value.zip);
    } else {
      obs$ = this.performDuplicateUserCheck(this.user.value.email)
        .pipe( switchMap(() => this.performDeliveryAreaCheck(this.user.value.zip)) );
    }

    obs$
      .subscribe(
        () => {
          loader.dismiss();
          const order = this.orderFlowService.getOrder();
          if (this.isLoggedIn && order.mealPlan) {
            this.orderFlowService.setZipCodeChanged(true);
            this.navigateToDeliveryDateSelection();
          } else {
            this.navigateToPlanSelection();
          }
        },
        (err) => {
          loader.dismiss();
          let error = get(err, 'error.message', null) ? err.error : null;
          if (!error) {
            error = APP_IS_ON_DEVICE ? JSON.parse(err.error) : err.error;
          }
          this.service.showToaster(error.message);
        }
      );
  }

  private performDeliveryAreaCheck(zip: string): Observable<any> {
    return this.registrationService.getDeliveryAreaForZipcode(zip)
      .snapshotChanges()
      .pipe(
        take(1),
        switchMap(data => {
          const isZipCodeInArea = data.length && data[0].payload.doc.data();
          if (isZipCodeInArea) {
            const city = data[0].payload.doc.id;
            return of(city);
          } else {
            return throwError({
              error: {
                message: 'Sorry, we don\'t currently offer service to that area'
              }
            });
          }
        }),
        tap(city => {
          const deliveryAddress = new Address();
          deliveryAddress.city = city;
          const update: Partial<Order> = this.isLoggedIn
            ? {
              zip: this.user.value.zip,
              deliveryAddress: deliveryAddress
            }
            : {
              email: this.user.value.email,
              zip: this.user.value.zip,
              deliveryAddress: deliveryAddress
            };
          this.orderFlowService.updateOrder(update);
        })
      );
  }

  private performDuplicateUserCheck(email: string): Observable<any> {
    return this.userService.checkIfUserIsDuplicate(email);
  }

  private navigateToPlanSelection() {
    this.navCtrl.navigateForward('/plan-selection');
  }

  private navigateToDeliveryDateSelection() {
    this.navCtrl.navigateForward('delivery-day-selection');
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    const pass = group.get('password').value;
    const confirmPass = group.get('confirmPassword').value;

    return pass === confirmPass ? null : { notSame: true }
  }

  navigateToLogin() {
    this.navCtrl.navigateRoot('/login');
  }

}


