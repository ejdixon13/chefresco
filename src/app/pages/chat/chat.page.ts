import { Service } from './../../services/app.service';
import { ViewChild, Component } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { IonContent } from '@ionic/angular';
import { ChatService } from "./chat.service";
import { Router } from '@angular/router';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
  providers: [ChatService]
})
export class ChatPage {
  @ViewChild(IonContent) content: IonContent;
  public noOfItems: number;

  chatMessage = {
    message: "",
    sendBy: "User",
    userName: "",
    createdAt: Date.now()
  };

  public chatList;
  public sevenDaysBack: number;
  public imageUrl: string = 'assets/img/profile.jpg';

  constructor(
    public loadingCtrl: LoadingController,
    public chatService: ChatService,
    public service: Service,
    public router: Router

  ) {
    const cartItems = JSON.parse(localStorage.getItem('cartItem'));
    if (cartItems != null) this.noOfItems = cartItems.length;
  }

  async ngOnInit() {
    let date = new Date();
    let midnight = date.setUTCHours(0, 0, 0, 0);
    this.sevenDaysBack = midnight - 7 * 24 * 60 * 60 * 1000;
    let loader = await this.service.showLoading();


    if (localStorage.getItem('uid') != null) {
      this.getUserInfo();
      this.getAllConversations(loader);
    } else {
      this.router.navigate(['/home']);
    }

  }

  getAllConversations(loader) {
    this.chatService.getChatList().valueChanges().subscribe((response: any) => {
      this.chatList = [];
      this.chatList = response;
      this.scrollToBottom();
      loader.dismiss();
    }, error => {
      loader.dismiss();
    })
  }

  getUserInfo() {
    this.service.getUserDetails(localStorage.getItem('uid')).valueChanges().subscribe((userInfo: any) => {
      this.chatMessage.userName = userInfo.name;
      this.imageUrl = userInfo.image != null ? this.imageUrl = userInfo.image : this.imageUrl = 'assets/img/profile.jpg';
    });
  }


  onSend() {
    if (this.chatMessage.message.length > 0) {
      this.chatService.sendMessage(this.chatMessage).then(response => {
        this.chatMessage.message = '';
        this.scrollToBottom();
      });
    }

  }

  scrollToBottom() {
    setTimeout(() => {
      this.content.scrollToBottom();
    }, 3000);
  }

}
