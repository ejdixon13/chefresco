import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  constructor(private afFS: AngularFirestore, private afAuth: AngularFireAuth) { }

  getChatList() {
    return this.afFS.collection('/messages/' + localStorage.getItem('uid'));
  }

  sendMessage(chatData) {
    return this.afFS.collection('/messages/' + localStorage.getItem('uid')).add(chatData);
  }

}
