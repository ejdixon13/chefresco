import { environment } from './../../../environments/environment';
import { Service } from 'src/app/services/app.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { CloudinaryOptions, CloudinaryUploader } from 'ng2-cloudinary';
import { SettingsService } from './settings.service';
import { Events, Platform } from '@ionic/angular';
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
  providers: [SettingsService]
})
export class SettingsPage implements OnInit {

  public showNotification: boolean = true;

  public user: any = {

    image: 'assets/img/profile.jpg'
  };
  public language: string;
  private flag: number = 0;
  public options = environment.languages;
  public cloudinaryUpload = environment.cloudinaryConfig;
  public uploader: CloudinaryUploader = new CloudinaryUploader(
    new CloudinaryOptions(this.cloudinaryUpload)
  );
  public imageUrl: string;
  public notification: boolean;

  constructor(
    public platform: Platform,
    public events: Events,
    public translate: TranslateService,
    private service: Service,
    private settingService: SettingsService
  ) {
    let language = localStorage.getItem('language');
    this.language = language != null ? language : 'en';
    let notification = localStorage.getItem('notification');
    this.notification = notification != null ? notification == 'true' : true;
    this.translate.setDefaultLang("en");
  }

  async ngOnInit() {
    let loader = await this.service.showLoading();
    this.service.getUserDetails(localStorage.getItem('uid')).valueChanges().subscribe((res: any) => {
      this.user = res;
      console.log("response of user" + JSON.stringify(res))
      this.user.image = res.image ? res.image : 'assets/img/profile.jpg';
      this.translate.use(this.language);
      loader.dismiss();
    }, error => {
      loader.dismiss();
    });
  }

  async onSubmit(user: NgForm) {
    let loader = await this.service.showLoading();
    if (this.flag == 1) {
      console.log("falg 1 if ")
      this.uploader.uploadAll();
      this.uploader.onSuccessItem = (item: any, response: string, status: number, headers: any): any => {
        let res: any = JSON.parse(response);
        this.user.image = res.secure_url;
        this.user.publicId = res.public_id;
        this.settingService.updateUserInfo(this.user._id, this.user).then((response: any) => {
          this.events.publish('profileInfo', { imageUrl: this.user.image, username: this.user.name });
          loader.dismiss();
          this.flag = 0;
          this.service.showToaster('Profile has been updated!');
        });
      }
    } else {
      console.log("falg else ")
      this.flag = 0;
      this.settingService.updateUserInfo(this.user._id, this.user).then((response: any) => {
        this.events.publish('profileInfo', { imageUrl: this.user.image ? this.user.image : 'assets/img/profile.jpg', username: this.user.name });
        loader.dismiss();
        this.service.showToaster('Profile has been updated!');
      });
    }
  }

  readUrl(event) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.onload = (event: any) => {
        this.user.image = event.target.result;
        this.flag = 1;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  changeLanguage() {
    if (this.language == 'fr') {
      this.translate.use('fr');
      // this.plateformDir='ltr';
      // this.plateformDir
    } else if (this.language == 'ar') {
      this.translate.use('ar');
      // this.plateformDir='rtl';
    }
    else {
      this.translate.use('en');
      // this.plateformDir='ltr';
    }
    localStorage.setItem('language', this.language);
    // localStorage.setItem("plateformDir",this.plateformDir);
  }

  // changeLanguage() {
  //   if (this.value == 'fr') {
  //     this.translate.use('fr');
  //     this.platform.setDir('ltr', true);
  //   } else if (this.value == 'ar') {
  //     this.platform.setDir('rtl', true);
  //     this.translate.use('ar');
  //   }
  //   else {
  //     this.translate.use('en');
  //     this.platform.setDir('ltr', true);
  //   }
  //   localStorage.setItem('language', this.value);

  // }

  changeSetting() {
    localStorage.setItem('showNotification', JSON.stringify(this.showNotification));
  }

}
