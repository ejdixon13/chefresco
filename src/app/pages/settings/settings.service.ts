import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})

export class SettingsService {

  constructor(private afFS: AngularFirestore, private adAuth: AngularFireAuth) {}

  updateUserInfo(userId, userInfo) {
    return this.afFS.doc('/users/' + localStorage.getItem('uid')).update(userInfo);
  }

  getSettings() {
    return this.afFS.collection('settings').doc('vJD9liyva02eTsEVIS8U');
  }

}
