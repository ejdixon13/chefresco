import { ConstService } from './../../services/constant.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ProductDetailsService {
  constructor(public http: HttpClient, public constService: ConstService,
    private afFS: AngularFirestore, private afAuth: AngularFireAuth) { }

  getMenuItemDetails(menuItemId) {
    return this.afFS.doc('menuItems/' + menuItemId);
  }

  addToFavourite(productId, menuItems: any) {
    return this.afFS.doc('/users/' + this.afAuth.auth.currentUser.uid + '/favourite/' + productId)
      .update({
        thumb: menuItems.thumb,
        title: menuItems.title,
        description: menuItems.description
      });
  }

  removeFromFavourite(productId) {
    return this.afFS.doc('/users/' + this.afAuth.auth.currentUser.uid + '/favourite/' + productId)
      .delete();
  }

  getUserFavouriteItems() {
    return this.afFS.collection('/users/' + this.afAuth.auth.currentUser.uid + '/favourite/');
  }

  checkFavourite(productId) {
    const body = {
      menuItem: productId
    };
    const authtoken = localStorage.getItem('token');
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', authtoken);
    return this.http.post(this.constService.base_url + 'api/favourites/check', body, {
      headers: headers
    });
  }

}
