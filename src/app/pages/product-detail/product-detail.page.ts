import { Service } from './../../services/app.service';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductDetailsService } from './product-details.service';
import { NavController, ToastController } from '@ionic/angular';
import { OrderFlowService, Customization } from '../../services/order-flow.service';
import { filter, find } from 'lodash';
import { FOOD_CATEGORY } from 'src/app/shared/constant';



@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
  providers: [ProductDetailsService, Service]
})
export class ProductDetailPage {

  public count = 1;
  public noOfItems: number;
  product: any = {
    name: '',
    sizeOption: {},
    extraOptions: [],
    specialRequests: '',
    quantity: 1
  };
  public productDetails: any = {};
  public isFavourite = false;
  public currency: any;
  private favouriteItems: any[] = [];
  private productId: any;
  private selectedSize: string;
  public isFromOffer = false;
  public previousRoute = 'product-list';
  public isMealSelected = false;
  constructor(
    private navCtrl: NavController,
    public activatedRoute: ActivatedRoute,
    public service: Service,
    public productDetailsService: ProductDetailsService,
    private orderFlowService: OrderFlowService,
    public toastController: ToastController
  ) {
    if (localStorage.getItem('isFromOffer') != null) {
      this.isFromOffer = Boolean(localStorage.getItem('isFromOffer'));
      this.previousRoute = 'offer';
    }

    this.activatedRoute.params.subscribe(id => {
      if (id.productId) {
        this.productId = id.productId;
        console.log('productId', id.productId);
        this.getMenuItemDetails(id.productId);
        // if (localStorage.getItem('uid')) {

        //   this.getFavouriteItems(id.productId);
        // } else {
        //   this.getMenuItemDetails(id.productId);
        // }
      } else {
        this.navCtrl.navigateRoot('/home');
      }

    });
    this.currency = localStorage.getItem('currency');

  }

  ionViewWillEnter() {
    const cart = JSON.parse(localStorage.getItem('cartItem'));
    if (cart != null) { this.noOfItems = cart.length; }
    const meal = this.orderFlowService.getMeal(this.productId);
    this.isMealSelected = !!meal;
    if (meal) {
      this.product.extraOptions = meal.customizations;
      this.product.specialRequests = meal.specialRequests;
      this.product.quantity = meal.quantity || 0;
    }
    console.log(this.noOfItems);
  }

  async getMenuItemDetails(productId) {
    if (localStorage.getItem('uid')) { this.checkfavourite(productId); }
    const loader = await this.service.showLoading();
    this.productDetailsService.getMenuItemDetails(productId).valueChanges()
      .subscribe((res: any) => {
        this.productDetails = res;
        this.productDetails._id = this.productId;
        console.log('product detail' + JSON.stringify(this.productDetails));
        this.product = {
          productId: this.productDetails._id,
          name: this.productDetails.title,
          imageUrl: this.productDetails.thumb,
          ratingFlag: 0,
          rating: 0,
          sizeOption: {},
          extraOptions: this.product.extraOptions.length ? this.product.extraOptions : [],
          specialRequests: this.product.specialRequests ? this.product.specialRequests : '',
          quantity: this.product.quantity !== 0 ? this.product.quantity : 1
        };
        loader.dismiss();
      }, error => {
        loader.dismiss();
        this.service.showToaster('Somthing went wrong');
      });

  }

  getFavouriteItems(productId) {
    this.productDetailsService.getUserFavouriteItems().snapshotChanges().subscribe((data: any) => {
      data.forEach(item => {
        const temp = item.payload.doc.data();
        temp['_id'] = item.payload.doc.id;
        this.favouriteItems.push(temp);
      });
      this.getMenuItemDetails(productId);
    });
  }

  onCheckExtraIngredients(option) {
    if (this.product.extraOptions.findIndex(extraOptions => option.name == extraOptions.name) == -1) {
      this.product.extraOptions.push(option);
    } else {
      this.product.extraOptions.splice(this.product.extraOptions.findIndex(extraOptions => option.name == extraOptions.name), 1);
    }
  }

  onSelectSize(price) {
    this.product.sizeOption = price;
    this.selectedSize = price.pname;
  }

  increaseProductCount() {
    if (this.count < 10) {
      this.count = this.count + 1;
    } else {
      this.service.showToaster('You can add upto 10 items only!');
    }
  }

  decreaseProductCount() {
    if (this.count > 1) {
      this.count = this.count - 1;
    } else {
      this.service.showToaster('At least one item required!');
    }
  }

  checkfavourite(productId) {
    const index = this.favouriteItems.findIndex(item => item._id == productId);
    if (index >= 0) {
      this.isFavourite = true;
    } else {
      this.isFavourite = false;
    }
    // this.productDetailsService.checkFavourite(productId).subscribe((res: any) => {
    //   this.isFavourite = res.resflag;
    // }, error => {
    //   this.service.showToaster(error.message);
    // });
  }

  async addToFavourite(productId) {

    // const loader = await this.service.showLoading();
    // if (localStorage.getItem('uid')) {
    //   this.productDetailsService.addToFavourite(productId, this.productDetails).then(() => {
    //     this.isFavourite = true;
    //     loader.dismiss();
    //     this.service.showToaster('Added to Favourites!');
    //   }).catch(error => {
    //     console.log(error);
    //   });
    // } else {
    //   this.service.showToaster('Please login first to add!', 3000);
    //   loader.dismiss();
    // }


  }

  async removeFromFavourite(productId) {
    // const loader = await this.service.showLoading();
    // if (localStorage.getItem('uid')) {
    //   this.productDetailsService.removeFromFavourite(productId).then(() => {
    //     this.isFavourite = false;
    //     this.service.showToaster('Removed from favourites!');
    //     loader.dismiss();
    //   }).catch(error => {
    //     loader.dismiss();
    //     this.service.showToaster('Somthing went worng');
    //   });
    // }

  }

  addToCart(productId) {
    console.log('product id----------size', productId, this.selectedSize);
    if (this.product.sizeOption && this.product.sizeOption.value) {
      let cart = JSON.parse(localStorage.getItem('cartItem'));
      if (cart == null) {
        cart = [];
        this.performAddOperation(cart, productId);
      } else {
        if (cart.findIndex(product => (productId == product.item.itemId) && (this.selectedSize == product.item.price.pname)) == -1) {
          this.performAddOperation(cart, productId);
        } else {
          console.log('else part ');
          const index = cart.findIndex(product => (productId == product.item.itemId) && (this.selectedSize == product.item.price.pname));
          // cart.splice(cart.findIndex(product => (productId == product.item.itemId) && (this.selectedSize == product.item.price.pname)) != -1, 1);
          cart.splice(index, 1);
          this.performAddOperation(cart, productId);
        }
      }
    } else {
      this.service.showToaster('Please Select The Size & Price');
    }
  }

  performAddOperation(cart, productId) {
    console.log('perform-------', productId);

    const productData: any = {
      item: {
        itemId: productId,
        itemQunatity: this.count,
        price: this.product.sizeOption,
        extraOptions: this.product.extraOptions,
        thumb: this.productDetails.thumb,
        title: this.productDetails.title
      },
      itemTotalPrice: null,
      extraPrice: null
    };
    // this.product.Quantity = this.count;
    let preExtraPrice = 0;
    this.product.extraOptions.forEach(option => {
      preExtraPrice = preExtraPrice + Number(option.value);
    });

    console.log('product sepecial price ', this.product.sizeOption.specialPrice);
    if (this.product.sizeOption.specialPrice != undefined) {
      productData.itemTotalPrice = this.count * this.product.sizeOption.specialPrice;
    } else {
      productData.itemTotalPrice = this.count * this.product.sizeOption.value;
    }
    productData.extraPrice = preExtraPrice;
    cart.push(productData);
    console.log('Cart Data' + JSON.stringify(cart));

    // this.product.extraPrice = preExtraPrice;
    // cart.push(this.product);
    localStorage.setItem('cartItem', JSON.stringify(cart));
    this.navCtrl.navigateForward('/cart');
  }

  isExtraOptionChecked(option: Customization) {
    return !!find(this.product.extraOptions, opt => opt.name === option.name);
  }

  async presentMealLimitToast() {
    const toast = await this.toastController.create({
      message: 'You have reached your meal limit. To add more, please adjust your meal quantity for the week.',
      duration: 2000
    });
    toast.present();
  }

  async selectAndNavigateBackToList() {
    if (this.hasHitMealQuantityLimit()) {
      await this.presentMealLimitToast();
      return;
    }
    const meal = this.orderFlowService.getMeal(this.productId);
    if (!meal) {
      this.orderFlowService.addMeal({
        mealId: this.productId,
        title: this.productDetails.title,
        thumbnailUrl: this.productDetails.thumb,
        quantity: this.product.quantity,
        customizations: this.product.extraOptions,
        specialRequests: this.product.specialRequests
      });
    }
    this.navCtrl.navigateForward('/product-list;menuId=' + FOOD_CATEGORY);
  }

  private hasHitMealQuantityLimit() {
    const numMealsSpecified = this.orderFlowService.getOrder().mealPlan.numMealsPerWeek;
    let quantity = this.orderFlowService.getAllMealsQuantity() + this.product.quantity; 
    if (this.orderFlowService.isMealAdded(this.productId)) {
      quantity -= this.orderFlowService.getMeal(this.productId).quantity;
    }
    return quantity > numMealsSpecified;
  }

  async makeEditAndNavigateBackToList() {
    if (this.hasHitMealQuantityLimit()) {
      await this.presentMealLimitToast();
      return;
    }
    const meal = this.orderFlowService.getMeal(this.productId);
    meal.customizations = this.product.extraOptions;
    meal.specialRequests = this.product.specialRequests;
    meal.quantity = this.product.quantity;
    this.navCtrl.navigateForward('/product-list;menuId=' + FOOD_CATEGORY);
  }

  getAllMealsQuantity() {
    return this.orderFlowService.getAllMealsQuantity();
  }
  getMaxNumMeals() {
    return this.orderFlowService.getOrder().mealPlan.numMealsPerWeek;
  }

  goToCartPage() {
    this.navCtrl.navigateForward('/cart');
  }

  subtractOneFromQuantity() {
    if (this.product.quantity > 0) {
      --this.product.quantity;
    }
  }

  addOneToQuantity() {
    if (this.product.quantity < this.getMaxNumMeals()) {
      ++this.product.quantity;
    }
  }

}
