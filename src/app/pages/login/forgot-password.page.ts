import { ActivatedRoute } from '@angular/router';
import { Events, NavController, Platform } from '@ionic/angular';
import { Service } from './../../services/app.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
// import { Facebook } from '@ionic-native/facebook';
// import * as firebase from "firebase";
// import { GooglePlus } from "@ionic-native/google-plus";
// import { TwitterConnect } from "@ionic-native/twitter-connect";


const ON_PASSWORD_RESET_EMAIL_MSG = 'An email has been sent with instructions to reset your password.';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./login.page.scss'],
  providers: [LoginService]

})
export class ForgotPasswordPage implements OnInit {
  public user: FormGroup;
  private routeInfo: string;

  constructor(
    public loginService: LoginService,
    public fb: FormBuilder,
    private route: ActivatedRoute,
    private events: Events,
    private navCtrl: NavController,
    private service: Service,
      private afAuth: AngularFireAuth,
    public platform: Platform,
  ) {
    this.route.params.subscribe((id) => {
      this.routeInfo = id.route;
    });
  }

  ngOnInit() {
    if (localStorage.getItem('uid')) {
      localStorage.removeItem('uid');
    }

    this.user = this.fb.group({
      email: ['', Validators.required], // auto fill the email-input-field.
    });
  }

    public async onPasswordReset() {
        const email = this.user.get('email').value;
        if (email) {
            const loader = await this.service.showLoading();

            this.afAuth.auth.sendPasswordResetEmail(this.user.get('email').value).then(() => {
                loader.dismiss();
                this.service.showToaster(ON_PASSWORD_RESET_EMAIL_MSG);
                this.navCtrl.navigateRoot('/login');
            }).catch((error) => {
                this.service.showToaster(error.message);
                loader.dismiss();
                console.log(error);
            });
        }
  }

  navigateTo() {
    this.navCtrl.navigateRoot('/registration');
  }

}
