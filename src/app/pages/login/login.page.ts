import { ActivatedRoute } from '@angular/router';
import { Events, NavController, Platform } from '@ionic/angular';
import { Service } from './../../services/app.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
// import { Facebook } from '@ionic-native/facebook';
// import * as firebase from "firebase";
// import { GooglePlus } from "@ionic-native/google-plus";
// import { TwitterConnect } from "@ionic-native/twitter-connect";


const INVALID_PASSWORD = 'INVALID_PASSWORD';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  providers: [LoginService]

})
export class LoginPage implements OnInit {
  public user: FormGroup;
  private routeInfo: string;

  constructor(
    public loginService: LoginService,
    public fb: FormBuilder,
    private route: ActivatedRoute,
    private events: Events,
    private navCtrl: NavController,
    private service: Service,
    private afAuth: AngularFireAuth,
    private afDB: AngularFireDatabase,
    // public facebook: Facebook,
    // public googlePlus: GooglePlus,
    // public twitter: TwitterConnect,
    public platform: Platform,
  ) {
    this.route.params.subscribe((id) => {
      this.routeInfo = id.route;
    });
  }

  ngOnInit() {
    if (localStorage.getItem('uid')) {
      localStorage.removeItem('uid');
    }

    this.user = this.fb.group({
      email: ['', Validators.required], // auto fill the email-input-field.
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])] // auto fill the password-input-field.
    });
  }

  public async onLogin() {
    const loader = await this.service.showLoading();

    this.loginService.login(this.user.value).then((res: any) => {
      localStorage.setItem('uid', res.user.uid);
      this.service.getUserDetails(res.user.uid).valueChanges().subscribe((response: any) => {
        this.events.publish('profileInfo', { imageUrl: response.imageUrl, username: response.name });
      });
      loader.dismiss();
      this.service.showToaster('Login Successful!');
      // if (this.routeInfo === 'cart') {
      //   this.navCtrl.navigateBack('/cart');
      // } else {
      // }
      this.navCtrl.navigateRoot('/home');
    }).catch(error => {
      this.service.showToaster(error.message);
      loader.dismiss();
      console.log(error);
    });
  }


  doFbLogin() {
    // let permissions = new Array();
    // permissions = ["public_profile", "email", "user_education_history"];
    // this.facebook.login(permissions).then(success => {
    //   this.facebook.api("/me?fields=id,name,email,gender,first_name,last_name", permissions).then(user => {
    //     var provider = firebase.auth.FacebookAuthProvider.credential(
    //       success.authResponse.accessToken
    //     );
    //     firebase.auth().signInWithCredential(provider).then((response: any) => {
    //       this.loginService.saveUserInfo(response).then(() => {
    //         localStorage.setItem("uid", response.uid);
    //         this.events.publish('profileInfo', { imageUrl: response.photoURL, username: response.displayName });
    //         if (this.routeInfo == 'cart') {
    //           this.navCtrl.navigateBack('/cart');
    //         } else {
    //           this.navCtrl.navigateRoot('/home');
    //         }
    //       }).catch(error => {
    //         this.service.showToaster(error.message);
    //       })

    //     }).catch(error => {
    //       console.log("fb Error1" + JSON.stringify(error));
    //       this.service.showToaster(error.message);
    //     });
    //   }),
    //     error => {
    //       // console.log("fb Error2", error);
    //     };
    // },
    //   error => {
    //     // console.log("FaceBook ERROR3 : ", error);
    //   });
  }

  public async  googleLogin() {
    // this.googlePlus.login({
    //   scopes: "",
    //   webClientId: "164935859218-0jlgn5jj6l5nml4lrchpmi7pquvihn09.apps.googleusercontent.com",
    //   offline: true
    // }).then(success => {
    //   var provider = firebase.auth.GoogleAuthProvider.credential(success.idToken);
    //   firebase.auth().signInWithCredential(provider).then(response => {
    //     this.loginService.saveUserInfo(response).then(() => {
    //       localStorage.setItem("uid", response.uid);
    //       this.events.publish('profileInfo', { imageUrl: response.photoURL, username: response.displayName });

    //       if (this.routeInfo == 'cart') {
    //         this.navCtrl.navigateBack('/cart');
    //       } else {
    //         this.navCtrl.navigateRoot('/home');
    //       }
    //     }).catch(error => {

    //       this.service.showToaster(error.message);
    //     })
    //   }).catch(error => {
    //     // console.log("gp Error1", error);
    //     this.service.showToaster(error.message);
    //   });
    // },
    //   error => {
    //     // console.log("gp Error2", error);
    //   });
  }

  twitterLogin() {
    // this.platform.ready().then(res => {
    //   if (res == "cordova") {
    //     this.twitter.login().then(result => {
    //       // console.log('twtter response1', result)
    //       this.twitter.showUser().then(user => {

    //         var provider = firebase.auth.TwitterAuthProvider.credential(result.token, result.secret);
    //         firebase.auth().signInWithCredential(provider).then(response => {
    //           this.loginService.saveUserInfo(response).then(() => {
    //             localStorage.setItem("uid", response.uid);
    //             this.events.publish('profileInfo', { imageUrl: response.photoURL, username: response.displayName });
    //             if (this.routeInfo == 'cart') {
    //               this.navCtrl.navigateBack('/cart');
    //             } else {
    //               this.navCtrl.navigateRoot('/home');
    //             }
    //           }).catch(error => {
    //             this.service.showToaster(error.message);
    //           })
    //         }).catch(error => {
    //           // console.log("Error1 ", error);
    //           this.service.showToaster(error.message);
    //         });
    //       },
    //         onError => {
    //           // console.log("error2", onError);
    //         });
    //     });
    //   }
    // });
  }

  navigateTo() {
    this.navCtrl.navigateRoot('/registration');
  }

  navigateToForgotPassword() {
    this.navCtrl.navigateForward('/login/forgot-password');
  }

}