import { ConstService } from './../../services/constant.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})

export class LoginService {

  constructor(private afAuth: AngularFireAuth, private afFS: AngularFirestore) { }

  login(user: any) {
    return this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);
  }

  saveUserInfo(user) {
    return this.afFS.doc('users/' + user.uid).set({
      name: user.displayName,
      email: user.email,
      image: user.photoURL,
      role: 'User'
    });
  }

  // send facebook user details of user to server
  loginUserViaFacebook(body) {
    // const headers = new HttpHeaders().set("Content-Type", "application/json");
    // return this.http.post(
    //   this.constService.base_url + "api/users/auth/facebook", body, { headers: headers }
    // );
  }



}
