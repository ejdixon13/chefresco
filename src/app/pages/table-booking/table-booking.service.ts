import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable()
export class BookTableService {

  private userId: any;
  constructor(private afFS: AngularFirestore,
    private afAuth: AngularFireAuth) {
    this.userId = afAuth.auth.currentUser.uid;
  }
  bookTable(tableInfo) {
    return this.afFS.collection('/table-bookings').add(tableInfo);
  }

}
