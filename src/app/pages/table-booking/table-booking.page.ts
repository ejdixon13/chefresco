import { Service } from 'src/app/services/app.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BookTableService } from './table-booking.service';
import { AlertController } from '@ionic/angular';



@Component({
  selector: 'app-table-booking',
  templateUrl: './table-booking.page.html',
  styleUrls: ['./table-booking.page.scss'],
  providers: [BookTableService]
})
export class TableBookingPage {
  public bookingInfo: any = {

    userID: '',
    time: '',
    date: Date,
    person: null,
    status: '',
  };
  public noOfItems: number;


  constructor(
    public router: Router,
    private service: Service,
    private alertCtrl: AlertController,
    private bookTableService: BookTableService
  ) {
    this.bookingInfo.userID = localStorage.getItem('uid');
    let cartItems = JSON.parse(localStorage.getItem("cartItem"));
    if (cartItems != null) this.noOfItems = cartItems.length;

  }

  async onClickBookTable() {

    if (this.bookingInfo.time && this.bookingInfo.date && this.bookingInfo.person != undefined) {
      this.presentConfirm();
    } else {
      this.service.showToaster('All filed Required to book table.');
    }
  }



  async presentConfirm() {
    const alert = await this.alertCtrl.create({
      header: 'Confirm Booking!',
      message: 'Sure? Do you want to book table?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (cancel) => {
            this.service.showToaster('Your canclled booking!');
            this.router.navigate(['/home']);
          }
        }, {
          text: 'Okay',
          handler: () => {
            console.log(this.bookingInfo);
            this.bookTableService.bookTable(this.bookingInfo).then(() => {
              this.service.showToaster('Your table booked!');
              this.router.navigate(['/home']);
            })
          }
        }
      ]
    });

    await alert.present();
  }


}
