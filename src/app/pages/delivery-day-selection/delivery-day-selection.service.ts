import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as moment from 'moment';
import { Observable, combineLatest } from 'rxjs';
import { map, switchMap, filter } from 'rxjs/operators';
import { take } from 'lodash';

export interface DeliveryOption {
    zipCodes: string[];
    deliveryDays: string[];
}

export interface DeliveryDate {
    date: moment.Moment;
    ordersRemaining: number;
}

@Injectable({
    providedIn: 'root'
})
export class DeliveryDaySelectionService {

    private DEFAULT_MAX_ORDERS;
    private CUT_OFF_NUM_DAYS_PRIOR_TO_DELIVERY: number;
    public AVERAGE_NUM_MEALS;
    private readonly MIN_SPOTS_REMAINING_BEFORE_SHOWING = 8;
    constructor(private afFS: AngularFirestore) {
        this.afFS.collection('settings')
            .get()
            .subscribe(data => {
                this.DEFAULT_MAX_ORDERS = data.docs[0].data().defaultMaxOrders;
                this.AVERAGE_NUM_MEALS = data.docs[0].data().averageNumMeals;
                this.CUT_OFF_NUM_DAYS_PRIOR_TO_DELIVERY = data.docs[0].data().cutOffNumDaysPriorToDelivery;
            });
    }

    /**
     * Determine number of spots remaining to show.
     * If below the threshold to show return the number otherwise return null
     * @param ordersRemaining
     */
    getSpotsRemainingToShow(ordersRemaining: number): number | null {
        const spotsRemaining = Math.floor(ordersRemaining / this.AVERAGE_NUM_MEALS);
        if (spotsRemaining <= this.MIN_SPOTS_REMAINING_BEFORE_SHOWING) {
            return spotsRemaining;
        } else {
            return null;
        }
    }

    getDeliveryDates(zipCode: string): Observable<DeliveryDate[]> {
        return this.afFS.collection('delivery-options', ref => ref.where('zipCodeList', 'array-contains', zipCode))
            .valueChanges()
            .pipe(
                map((data: any) => this.getMomentDeliveryDates(data[0].deliveryDays)),
                switchMap((dates) => this.getOrCreateDeliveryDates(dates))
            );
    }

    getNextAvailableDeliveryDate(zipCode: string): Observable<DeliveryDate[]> {
        return this.afFS.collection('delivery-options', ref => ref.where('zipCodeList', 'array-contains', zipCode))
            .valueChanges()
            .pipe(
                map((data: any) => this.getNextAvailableMomentDeliveryDate(data[0].deliveryDays)),
                filter(date => !!date),
                switchMap((date) => this.getOrCreateDeliveryDates([date]))
            );
    }

    private getNextAvailableMomentDeliveryDate(deliveryDays: string[] = []): moment.Moment {
        let availableDate = null;
        for (let idx = 0; idx < 2; idx++) {
            if (idx === 0) {
                availableDate = deliveryDays
                    .map(day => moment().day(day))
                    .find(momentDate => this.isDateWithinCutOffDate(momentDate));
            } else {
                availableDate = deliveryDays
                    .map(day => moment().day(day).add(idx, 'week'))
                    .find(momentDate => this.isDateWithinCutOffDate(momentDate));
            }
            if (availableDate) {
                return availableDate;
            }
        }
        return availableDate;
    }

    private getMomentDeliveryDates(deliveryDays: string[] = []): moment.Moment[] {
        const dates = [];
        deliveryDays.forEach(day => {
            const firstDate = moment().day(day);
            const secondDate = firstDate.clone().add(1, 'week');
            const thirdDate = secondDate.clone().add(1, 'week');
            if (this.isDateWithinCutOffDate(firstDate)) {
                dates.push(firstDate);
            }
            if (this.isDateWithinCutOffDate(secondDate)) {
                dates.push(secondDate);
            }
            dates.push(thirdDate);
        });
        return take(dates.sort((a, b) => a - b), 4);
    }

    private isDateWithinCutOffDate(date: moment.Moment): boolean {
        const todaysDate = moment();
        const cutOffDate = date.clone().subtract(this.CUT_OFF_NUM_DAYS_PRIOR_TO_DELIVERY, 'days');
        return cutOffDate.isSameOrAfter(todaysDate);
    }

    private getOrCreateDeliveryDates(dates: moment.Moment[]): Observable<DeliveryDate[]> {
        const formattedDates = dates.map(date => date.format('YYYY-MM-DD'));
        const obsList$ = formattedDates.map(date => this.afFS.collection('numOrders', ref => ref.where('date', '==', date)).get());
        return combineLatest(obsList$)
            .pipe(
                map(results => this.parseOrCreateDeliveryDates(results, dates))
            );
    }

    private parseOrCreateDeliveryDates(results: any[], dates: moment.Moment[]): DeliveryDate[] {
        const deliveryDates: DeliveryDate[] = [];
        results.forEach((result, index) => {
            if (result.docs && result.docs.length) {
                const ordersRemaining = result.docs[0].data().maxOrders - result.docs[0].data().totalOrders;
                if (ordersRemaining > this.AVERAGE_NUM_MEALS) {
                    deliveryDates.push({
                        date: dates[index],
                        ordersRemaining: ordersRemaining
                    });
                }
            } else {
                // this.createDeliveryDateInStore(dates[index]);
                deliveryDates.push({
                    date: dates[index],
                    ordersRemaining: this.DEFAULT_MAX_ORDERS
                });
            }
        });
        return deliveryDates;
    }

    private createDeliveryDateInStore(date: moment.Moment) {
        const formattedDate = date.format('YYYY-MM-DD');
        this.afFS.collection('numOrders').doc(formattedDate).set({
            date: formattedDate,
            maxOrders: this.DEFAULT_MAX_ORDERS,
            totalOrders: 0
        });
    }
}
