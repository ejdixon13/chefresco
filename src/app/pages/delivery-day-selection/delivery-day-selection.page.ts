
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { DeliveryDaySelectionService, DeliveryDate } from './delivery-day-selection.service';
import { OrderFlowService } from '../../services/order-flow.service';
import * as moment from 'moment';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FOOD_CATEGORY } from 'src/app/shared/constant';

@Component({
    selector: 'app-delivery-day-selection',
    templateUrl: './delivery-day-selection.page.html',
    styleUrls: ['./delivery-day-selection.page.scss'],
    providers: []
})
export class DeliveryDaySelectionPage implements OnInit {
    deliveryDates$: Observable<DeliveryDate[]>;
    chosenDate: moment.Moment;
    constructor(
        private navCtrl: NavController,
        private deliveryDayService: DeliveryDaySelectionService,
        private orderFlowService: OrderFlowService) { }
    ngOnInit() {
        const zipCode = this.orderFlowService.getOrder().zip;
        this.deliveryDates$ = this.deliveryDayService.getDeliveryDates(zipCode)
            .pipe( tap(dates => this.chosenDate = dates[0].date));
    }


    chooseDeliveryDate(date: moment.Moment) {
        this.chosenDate = date;
        this.orderFlowService.updateOrder({
            deliveryDate: this.chosenDate
        });
    }

    navigateToChooseMeals() {
        this.chooseDeliveryDate(this.chosenDate);
        this.navCtrl.navigateForward('/product-list;menuId=' + FOOD_CATEGORY);
    }

    getSpotsRemaining(ordersRemaining: number): number | null {
        return this.deliveryDayService.getSpotsRemainingToShow(ordersRemaining);
    }
}
