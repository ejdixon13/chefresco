import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslaterCustomModule } from './../../../app/translate.module';
import { DeliveryDaySelectionPage } from './delivery-day-selection.page';

const routes: Routes = [
  {
    path: '',
    component: DeliveryDaySelectionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslaterCustomModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DeliveryDaySelectionPage]
})
export class DeliveryDaySelectionPageModule { }
