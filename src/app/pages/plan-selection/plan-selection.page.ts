
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { OrderFlowService, PlanOption } from '../../services/order-flow.service';
import { PlanSelectionService } from './plan-selection.service';
import { Observable } from 'rxjs';


@Component({
    selector: 'app-plan-selection',
    templateUrl: './plan-selection.page.html',
    styleUrls: ['./plan-selection.page.scss'],
    providers: []
})
export class PlanSelectionPage implements OnInit {

    constructor(
        private navCtrl: NavController,
        private planSelectionService: PlanSelectionService,
        private orderFlowService: OrderFlowService) {
    }

    options$: Observable<PlanOption[]>;

    ngOnInit() {
        this.options$ = this.planSelectionService.getMealPlans();
    }

    choosePlan(option: PlanOption) {
        this.orderFlowService.updateOrder({
            mealPlan: option
        });
        this.navCtrl.navigateForward('delivery-day-selection');
    }

    getWeeklyMealPrice(option: PlanOption) {
        return Math.ceil(option.pricePerMeal * option.numMealsPerWeek);
    }
}
