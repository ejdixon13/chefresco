import { Injectable } from '@angular/core';
import { PlanOption } from '../../services/order-flow.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { orderBy } from 'lodash';

@Injectable({providedIn: 'root'})
export class PlanSelectionService {
    constructor(private afFS: AngularFirestore) { }

    getMealPlans(): Observable<PlanOption[]> {
        return this.afFS.collection('mealPlans').snapshotChanges()
            .pipe(map(collection => {
                const options = collection.map((item: any) => ({
                    id: item.payload.doc.id,
                    ...item.payload.doc.data() as PlanOption
                }));
                return orderBy(options, ['numMealsPerWeek']);
            }));
    }
}
