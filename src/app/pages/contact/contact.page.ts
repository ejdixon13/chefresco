import { Component } from '@angular/core';
import { NgForm } from "@angular/forms";
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { SettingsService } from '../settings/settings.service';
import { map, distinctUntilChanged } from 'rxjs/operators';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
  providers: [EmailComposer]
})
export class ContactPage {
  public user = {
    name: null,
    email: null,
    message: null,
    userId: null
  };
  public noOfItems: number;

  settings: any;
  constructor(private emailComposer: EmailComposer,
    private settingsService: SettingsService,
    private afAuth: AngularFireAuth,
    private afFS: AngularFirestore) {

    const cartItems = JSON.parse(localStorage.getItem('cartItem'));
    if (cartItems != null) { this.noOfItems = cartItems.length; }
    this.settingsService.getSettings().get().pipe(
      map(settingsRef => settingsRef.data()),
      distinctUntilChanged()
    ).subscribe(settings => this.settings = settings);
  }


  onSend(user: NgForm) {

    if (this.afAuth.auth.currentUser) {
      this.user.userId = this.afAuth.auth.currentUser.uid;
      this.afFS
        .collection('/contact')
        .add(this.user)
        .then(res => {
          this.user = {
            name: null,
            email: null,
            message: null,
            userId: null
          };
        });
    }
    this.emailComposer.open({
      // You just need to change this Email address to your own email where you want to receive email.
      to: 'ionicfirebaseapp@gmail.com',
      subject: this.user.name,
      body: this.user.message,
      isHtml: true
    }, () => { });
    this.user = {
      name: null,
      email: null,
      message: null,
      userId: null
    };
  }

}
