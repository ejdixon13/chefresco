import { Service } from 'src/app/services/app.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrdersService } from '../orders/orders.service';

@Component({
  selector: 'app-order-status',
  templateUrl: './order-status.page.html',
  styleUrls: ['./order-status.page.scss'],
  providers: [OrdersService]
})
export class OrderStatusPage implements OnInit {

  public order;
  public orderId: string;
  constructor(private orderDetailsService: OrdersService, private route: ActivatedRoute, private service: Service) { }

  async ngOnInit() {
    let loader = await this.service.showLoading();
    this.route.params.subscribe((id) => {
      this.orderId = id.orderId;
      this.orderDetailsService.getOrderDetailById(id.orderId).valueChanges().subscribe((res) => {
        console.log("Order Status" + JSON.stringify(res))
        this.order = res;
        loader.dismiss();
      });
    });
  }

}
