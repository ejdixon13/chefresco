import { Component, OnInit } from '@angular/core';
import { NewsService } from './news.service';
import { NavController } from '@ionic/angular';
import { Service } from 'src/app/services/app.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
  providers: [NewsService]
})
export class NewsPage implements OnInit {
  public newsList;

  constructor(
    private navCtrl: NavController,
    private service: Service,
    public newsService: NewsService
  ) { }

  async ngOnInit() {
    let loader = await this.service.showLoading();
    this.newsService.getNews().snapshotChanges().subscribe((data: any) => {
      this.newsList = [];
      data.forEach(item => {
        const temp = item.payload.doc.data();
        temp['_id'] = item.payload.doc.id;
        this.newsList.push(temp);
      });
      loader.dismiss();
    }, error => {
      loader.dismiss();
      this.service.showToaster(error.message);
    });
  }

  newsDetail(newsId) {
    this.navCtrl.navigateForward('/news-detail;newsId=' + newsId);
  }

}
