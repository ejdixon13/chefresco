import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable()
export class NewsService {

  constructor(private afFS: AngularFirestore) { }

  getNews() {
    return this.afFS.collection('news');
  }

  getNewsDetail(newsId) {
    return this.afFS.doc('/news/' + newsId);
  }

}
