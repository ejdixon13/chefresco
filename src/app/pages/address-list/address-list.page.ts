import { Service } from './../../services/app.service';
import { Component, OnInit } from '@angular/core';
import { AddressListService } from './address-list.service';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-address-list',
  templateUrl: './address-list.page.html',
  styleUrls: ['./address-list.page.scss'],
  providers: [AddressListService]
})
export class AddressListPage implements OnInit {
  public addressList;
  public orderInfo;
  public orderInfoCopy;
  public pincodes;
  public currency;
  public loyaltyInfo;
  public loyaltyPoints = 0;
  public checked: boolean = false;
  public remainingPoints;
  private isPincodeAvailable: boolean = false;
  private loyaltyArray: any[] = [];

  constructor(
    private navCtrl: NavController,
    private addressListService: AddressListService,
    public service: Service,
  ) {

  }

  async ngOnInit() {
    this.currency = localStorage.getItem('currency');
    if (localStorage.getItem('orderInfo')) { this.orderInfo = JSON.parse(atob(localStorage.getItem('orderInfo'))); }
    this.orderInfoCopy = JSON.parse(JSON.stringify(this.orderInfo));
    const loader = await this.service.showLoading();

    await this.addressListService.getAddressList().snapshotChanges().subscribe((data: any) => {
      this.addressList = [];
      data.forEach(item => {
        const temp = item.payload.doc.data();
        temp['_id'] = item.payload.doc.id;
        this.addressList.push(temp);
      });
    }, (error) => {
      this.service.showToaster('Error on getting address list');
    });

    await this.addressListService.getAvailablePincodes().valueChanges().subscribe((result: any) => {
      this.pincodes = result;
    }, (error) => {
      this.service.showToaster('Error onn getting pincodes');
    });

    // await this.addressListService.getLoyaltyStatus().valueChanges().subscribe((loyalty: any) => {
    //   this.loyaltyInfo = loyalty;
    // }, (error) => {
    //   this.service.showToaster(error.message);
    // });

    // await this.addressListService.getUserLoyalityPoints().valueChanges().subscribe((res: any) => {
    //   // this.loyaltyPoints = user.totalLoyaltyPoints;
    //   let points: any = res;
    //   this.loyaltyArray = points;
    //   let _points: number = 0;
    //   for (let i = 0; i < this.loyaltyArray.length; i++) {
    //     _points = Number(
    //       Number(_points) + Number(this.loyaltyArray[i].points)
    //     );
    //     this.loyaltyPoints = _points;
    //   }
    // }, error => {
    //   this.service.showToaster(error.message);
    // });

    loader.dismiss();
    this.orderInfo.status = 'pending';
  }

  async deleteAdress(id, index) {
    let loader = await this.service.showLoading();
    console.log(id)
    this.addressListService.deleteAdressById(id).then(() => {
      loader.dismiss();
    }).catch(error => {
      loader.dismiss();
      this.service.showToaster('Somthing went worng');
    })

  }

  editAdress(id) {
    this.navCtrl.navigateForward('/address;id=' + id);
  }

  addAddress() {
    this.navCtrl.navigateForward('/address;id=' + 0);
  }

  updateLoyalty(event) {
    if (this.loyaltyInfo.enable) {
      if (this.loyaltyPoints >= this.loyaltyInfo.minLoyalityPointes) {
        this.checked = event.detail.checked;
        if (event.detail.checked == true) {
          if (this.orderInfo.grandTotal <= this.loyaltyPoints) {
            this.remainingPoints = this.loyaltyPoints - this.orderInfo.grandTotal;
            this.orderInfo.grandTotal = 0;
          }
          else if (this.orderInfo.grandTotal > this.loyaltyPoints) {
            this.orderInfo.grandTotal = this.orderInfo.grandTotal - this.loyaltyPoints;
            this.remainingPoints = 0;
          }
        } else {
          this.remainingPoints = this.loyaltyPoints;
          this.orderInfo = JSON.parse(JSON.stringify(this.orderInfoCopy));
        }
      }
    }
  }

  selectAddress(address) {
    let availablePins = '';
    this.orderInfo.shippingAddress = address;
    delete this.orderInfo.shippingAddress['_id'];
    if (this.pincodes && this.pincodes.length > 0) {
      this.pincodes.forEach((pin, i) => {
        availablePins = availablePins + pin.pincode + ', ';
        if (pin.pincode == address.pincode) {
          this.isPincodeAvailable = true;
        }
        if (i == this.pincodes.length - 1 && !this.isPincodeAvailable) {
          this.service.showAlert('Delivery is available to these pincodes only (' + availablePins.slice(0, availablePins.length - 2) + ') !!');
        }
      });
    }
  }

  checkOut() {
    if (this.isPincodeAvailable) {
      this.orderInfo.appliedLoyalty = this.checked;
      if (this.orderInfo.appliedLoyalty == true) {
        this.orderInfo.usedLoyaltyPoints = this.loyaltyPoints - this.remainingPoints;
      }
      if (this.orderInfo.shippingAddress != null) {
        localStorage.setItem('orderInfo', btoa(JSON.stringify(this.orderInfo)));
        this.navCtrl.navigateForward('/checkout');
      }
      else {
        this.service.showToaster('Please select address first!');
      }
    } else {
      this.service.showToaster('Please select/change address first!');
    }
  }


}
