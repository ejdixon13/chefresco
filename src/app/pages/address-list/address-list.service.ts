import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class AddressListService {

    constructor(private afFS: AngularFirestore, private afAuth: AngularFireAuth) { }

    getAddressList() {
        return this.afFS.collection('/users/' + this.afAuth.auth.currentUser.uid + '/address')
    }

    getAvailablePincodes() {
        return this.afFS.collection('delivery-options');
    }

    getLoyaltyStatus() {
        return this.afFS.doc('loyalitys');
    }

    getUserLoyalityPoints() {
        return this.afFS.collection('users/' + this.afAuth.auth.currentUser.uid + '/loyaltyPoints');
    }

    deleteAdressById(addressId) {
        return this.afFS.collection('/users/' + this.afAuth.auth.currentUser.uid + '/address').doc(addressId).delete();
    }

}
