import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class CategoryService {

  constructor(public afFS: AngularFirestore) { }

  getCategories() {
    return this.afFS.collection('categories');
  }


}
