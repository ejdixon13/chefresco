import { Service } from './../../services/app.service';
import { Component } from '@angular/core';
import { CategoryService } from './category.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
  providers: [CategoryService]
})
export class CategoryPage {

  public categories;
  public noOfItems: number;

  constructor(
    private navCtrl: NavController,
    private service: Service,
    public categoryService: CategoryService
  ) {
    const cartItems = JSON.parse(localStorage.getItem('cartItem'));
    if (cartItems != null) { this.noOfItems = cartItems.length; }
    this.getCategories();
  }

  async getCategories() {
    const loader = await this.service.showLoading();
    this.categoryService.getCategories().snapshotChanges().subscribe(data => {
      this.categories = [];
      data.forEach(item => {
        const temp = item.payload.doc.data();
        temp['_id'] = item.payload.doc.id;
        this.categories.push(temp);
      });
      loader.dismiss();
    }, error => {
      loader.dismiss();
      this.service.showToaster('Somthing went wrong', 4000);
    });
  }

  navigateTo(menuId) {
    this.navCtrl.navigateForward('/product-list;menuId=' + menuId);
  }

}
