import { Component, OnInit } from '@angular/core';

declare const google;



@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
})
export class LocationPage implements OnInit {


  private title: string = 'Pietech Solution';
  private lat: number = 12.918844;
  private lng: number = 77.610877;
  private zoom: number = 12;



  constructor() { }

  ngOnInit() {
    this.initializeMap();
  }

  initializeMap() {
    var mapOptions = {
      center: new google.maps.LatLng(this.lat, this.lng),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      streetViewControl: false,
      disableDefaultUI: true
    };
    const latlngbounds = new google.maps.LatLngBounds();
    const gMap = new google.maps.Map(document.getElementById("dvMapEmergency"), mapOptions);
    var myLatlng = new google.maps.LatLng(this.lat, this.lng);
    var marker = new google.maps.Marker({
      position: myLatlng,
      map: gMap,
      title: this.title,
      draggable: true,
      animation: google.maps.Animation.DROP
    });
    latlngbounds.extend(marker.position);
    gMap.setCenter(latlngbounds.getCenter());
    gMap.fitBounds(latlngbounds);
    google.maps.event.addListenerOnce(gMap, 'bounds_changed', function (event) {
      if (this.getZoom()) {
        this.setZoom(12);
      }
    });
  }



}
