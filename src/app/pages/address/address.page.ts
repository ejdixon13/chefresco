import { Service } from './../../services/app.service';
import { Component } from '@angular/core';
import { AddressService } from './address.service';
import { NavController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'page-address',
  templateUrl: 'address.page.html',
  providers: [AddressService]
})

export class AddressPage {
  public address: any = {
    name: '',
    city: '',
    pincode: '',
    area: '',
    mobileNo: null,
    address: ''
  };
  public addressId: string;
  public isUpdateMode: boolean = false;

  constructor(public addressService: AddressService,
    private route: Router,
    private activatedRoute: ActivatedRoute,
    private navCtrl: NavController,
    private service: Service) {
    this.activatedRoute.params.subscribe((res: any) => {
      if (res.id != 0) {
        this.addressId = res.id;
        this.getAddressBYId();
      }
    })
  }

  ngOnInit() {

  }

  getAddressBYId() {
    if (this.addressId) {
      this.addressService.getAddressById(this.addressId).valueChanges().subscribe((response: any) => {
        this.address = response;
        this.isUpdateMode = true;
      });
    }
  }

  onSubmitAddress() {
    const mobileNo = this.address.mobileNo + '';
    if (mobileNo.length == 10) {
      if (this.isUpdateMode) {
        this.addressService.updateAddressById(this.addressId, this.address).then(response => {
          this.service.showToaster('Address updated successfully!');
          this.navCtrl.navigateBack('/address-list');
        }, (error) => {
          this.service.showToaster(error.message);
        });
      } else {
        this.addressService.addAddress(this.address).then(response => {
          this.service.showToaster('Address updated successfully!');
          this.navCtrl.navigateBack('/address-list');
        }, (error) => {
          this.service.showToaster(error.message);
        });
      }
    } else {
      this.service.showToaster('Enter 10 digit mobile no.');
    }

  }

}
