
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
    providedIn: 'root'
})

export class AddressService {

    constructor(private afAuth: AngularFireAuth, private afFS: AngularFirestore) { }

    addAddress(body) {
        return this.afFS.collection('/users/' + this.afAuth.auth.currentUser.uid + '/address')
            .add(body);
    }

    updateAddressById(addressId, address) {
        return this.afFS.doc('/users/' + this.afAuth.auth.currentUser.uid + '/address/' + addressId).update(address);
    }

    getAddressById(addressId) {
        return this.afFS.doc('/users/' + this.afAuth.auth.currentUser.uid + '/address/' + addressId);
    }

}