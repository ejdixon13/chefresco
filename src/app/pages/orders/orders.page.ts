import { Service } from './../../services/app.service';
import { Component, OnInit } from '@angular/core';
import { OrdersService } from './orders.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
  providers: [OrdersService]
})
export class OrdersPage implements OnInit {

  public orders;
  public currency;
  public noOfItems: number;
  constructor(
    public navCtrl: NavController,
    private service: Service,
    private orderService: OrdersService) {
    this.currency = localStorage.getItem('currency');
  }

  ionViewWillEnter() {
    const cart = JSON.parse(localStorage.getItem('cartItem'));
    if (cart != null) {
      this.noOfItems = cart.length;
    }
  }


  async ngOnInit() {

    const loader = await this.service.showLoading();
    // this.orderService.getOrders().snapshotChanges().subscribe((data: any) => {
    //   this.orders = [];
    //   data.forEach(item => {
    //     const temp = item.payload.doc.data();
    //     temp['_id'] = item.payload.doc.id;
    //     this.orders.push(temp);
    //   });
    //   loader.dismiss();
    //   console.log('Order Data' + JSON.stringify(this.orders));
    // }, error => {
    //   loader.dismiss();
    //   this.service.showToaster('Somthing went wrong', 4000);
    // });
  }

  orderDetails(orderId) {
    this.navCtrl.navigateForward('order-details;orderId=' + orderId);
  }

  goToCartPage() {
    this.navCtrl.navigateForward('/cart');
  }

}
