
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as moment from 'moment';



@Injectable({
  providedIn: 'root'
})

export class OrdersService {
  constructor(private afFS: AngularFirestore) {
  }

  getOrders(userId: string) {
    return this.afFS.collection('/orders', ref => ref.where('userId', '==', userId));
  }

  getCurrentOrders(userId: string) {
    const today = moment().format('YYYY-MM-DD');
    return this.afFS.collection('/orders', ref => ref.where('userId', '==', userId).where('deliveryDate', '>=', today));
  }

  getOrderDetailById(orderId) {
    return this.afFS.doc('/orders/' + orderId);
  }


}
