import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class CartService {

    constructor(private afFS: AngularFirestore) { }

    getCoupons() {
        return this.afFS.collection('coupons/');
    }

    getTaxSettingData() {
        return this.afFS.collection('settings');
    }

}
