import { Service } from './../../services/app.service';
import { Component } from '@angular/core';
import { CartService } from './cart.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
  providers: [CartService]
})
export class CartPage {

  public cartItems = [];
  public subTotalPrice: number;
  public taxAmount: number;
  public grandTotal: number;
  public noOfItems = 0;
  public globalTax = 0;
  public currency;
  public coupons;
  public couponDiscount = 0;
  public deductedPrice: number;
  public productId: string;
  public menuId: string;

  constructor(
    private navCtrl: NavController,
    private service: Service,
    private cartService: CartService
  ) {
    this.initializeCart();
  }

  async initializeCart() {
    const loader = await this.service.showLoading();
    this.currency = localStorage.getItem('currency');
    this.cartItems = JSON.parse(localStorage.getItem('cartItem'));
    if (this.cartItems != null) {
      this.noOfItems = this.cartItems.length;
      await this.getCouponsInfo();
      await this.getTaxInfo();
      await this.calculatePrice();
    }
    loader.dismiss();
  }

  async getCouponsInfo() {
    await this.cartService.getCoupons().valueChanges().subscribe((coupons: any) => {
      this.coupons = coupons;
    });
    // await this.cartService.getCoupons().subscribe((coupons: any) => {
    // 	this.coupons = coupons;
    // });
  }

  async getTaxInfo() {
    await this.cartService.getTaxSettingData().valueChanges()
      .subscribe((res: any) => {
        this.globalTax = res[0].taxPercent ? res[0].taxPercent : 0;
        this.calculatePrice();
      });
  }

  calculatePrice() {
    localStorage.setItem('cartItem', JSON.stringify(this.cartItems));
    this.cartItems = JSON.parse(localStorage.getItem('cartItem'));
    this.noOfItems = this.cartItems.length;
    if (this.cartItems) {
      let subTotal = 0;
      this.cartItems.forEach((data: any) => {
        if (data.item.price.specialPrice != undefined) {
          data.item.itemTotalPrice = data.item.itemQunatity * data.item.price.specialPrice;
        } else {
          data.item.itemTotalPrice = data.item.itemQunatity * data.item.price.value;
        }
        subTotal = subTotal + data.item.itemTotalPrice + data.extraPrice;
      });
      this.subTotalPrice = Number(subTotal.toFixed(2));
      this.taxAmount = Number(((this.globalTax / 100) * this.subTotalPrice).toFixed(2));
      this.deductedPrice = Number((this.couponDiscount / 100 * this.subTotalPrice).toFixed(2));
      this.subTotalPrice = this.subTotalPrice - this.deductedPrice;
      this.grandTotal = Number((this.subTotalPrice + this.taxAmount).toFixed(2));
    }
    const amountDetails = {
      grandTotal: this.grandTotal,
      subTotal: this.subTotalPrice,
      tax: this.taxAmount,
      couponDiscount: this.couponDiscount,
      deductedPrice: this.deductedPrice
    };
    localStorage.setItem('orderInfo', btoa(JSON.stringify(amountDetails)));
    localStorage.setItem('cartItem', JSON.stringify(this.cartItems));
  }

  removeCartItem(index) {
    // this.cartItems.forEach((data, i) => {
    // 	if (data.item.itemId == productId) {
    // 		this.cartItems.splice(i, 1);
    // 	}
    // });
    this.cartItems.splice(index, 1);
    this.calculatePrice();
  }

  increaseCartItem(index) {
    // console.log("product id", productId)
    // this.cartItems.forEach((data) => {
    // 	if (data.item.itemId == productId) {
    // 		if (data.item.itemQunatity < 10) {
    // 			data.item.itemQunatity = data.item.itemQunatity + 1;
    // 		}
    // 	}
    // });
    if (this.cartItems[index].item.itemQunatity < 10) {
      this.cartItems[index].item.itemQunatity = this.cartItems[index].item.itemQunatity + 1;
    }
    this.calculatePrice();
  }

  decreaseCartItem(index) {
    // this.cartItems.forEach((data) => {
    // 	if (data.item.itemId == productId) {
    // 		if (data.item.itemQunatity > 1) {
    // 			data.item.itemQunatity = data.item.itemQunatity - 1;
    // 		}
    // 	}
    // });
    if (this.cartItems[index].item.itemQunatity > 1) {
      this.cartItems[index].item.itemQunatity = this.cartItems[index].item.itemQunatity - 1;
    }
    this.calculatePrice();
  }

  navigateTo() {
    if (localStorage.getItem('uid')) {
      this.navCtrl.navigateForward('/address-list;route=cart');
    } else {
      this.navCtrl.navigateForward('/login');
    }
  }

}
