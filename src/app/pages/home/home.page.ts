import { Component, OnInit, OnDestroy } from '@angular/core';
import { HomeService } from './home.service';
import { Service } from '../../services/app.service';
import { NavController } from '@ionic/angular';
import { Order, OrderFlowService } from 'src/app/services/order-flow.service';
import * as moment from 'moment';
import { OrdersService } from '../orders/orders.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserService, User } from 'src/app/services/user.service';
import { DeliveryDaySelectionService, DeliveryDate } from '../delivery-day-selection/delivery-day-selection.service';
import { tap, takeUntil, map } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { get } from 'lodash';
import { MenuItemService } from 'src/app/services/menuItem.service';


interface Feature {
  key: string;
  thumb: string;
  title: string;
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [HomeService]
})
export class HomePage implements OnInit, OnDestroy {
  public featured = JSON.parse(`[{
    "key": "1",
    "thumb": "https://res.cloudinary.com/ionicfirebaseapp/image/upload/c_scale,h_220,w_420/v1483635589/shutterstock_56500270_j3dvz7.jpg",
    "title": "Special 30"
  },
  {
    "key": "2",
    "thumb": "https://res-1.cloudinary.com/ionicfirebaseapp/image/upload/c_scale,h_220,w_420/v1483635624/shutterstock_114177826_mm6tnl.jpg",
    "title": "50% off"
  },
  {
    "key": "3",
    "thumb": "https://res-2.cloudinary.com/ionicfirebaseapp/image/upload/c_scale,h_220,w_420/v1483635648/shutterstock_93878743_rugqzs.jpg",
    "title": "Lunch Special"
  }]`);

  featured$: Observable<Feature[]>;
  orders: Order[] = [];

  moment = moment;
  nextAvailableDeliveryDate: DeliveryDate;
  spotsLeftForNextAvailableDate: number;
  user: User;

  private ngUnsubscribe = new Subject();

  constructor(
    public homeService: HomeService,
    public service: Service,
    private navCtrl: NavController,
    private orderFlowService: OrderFlowService,
    private orderService: OrdersService,
    private afAuth: AngularFireAuth,
    private router: Router,
    private userService: UserService,
    private deliveryDateService: DeliveryDaySelectionService,
    private menuItemService: MenuItemService
  ) {
    // this.getCategories();
  }

  async ngOnInit() {
    const loader = await this.service.showLoading();
    this.afAuth.authState
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        user => {
          if (user) {
            loader.dismiss();
            this.refreshOrders(user);
            this.userDataSubscription();
          } else {
            loader.dismiss();
          }
        },
        () => {
          loader.dismiss();
          this.router.navigate(['/sign-up']);
        }
    );
    this.featured$ = this.menuItemService.getFeaturedMenuItems()
      .valueChanges()
      .pipe(
        map(data => {
          return data.map((featuredItem: any, idx) => ({
            key: idx.toString(),
            title: featuredItem.title,
            thumb: featuredItem.thumb
          }));
        })
      );
  }

  private userDataSubscription() {
    this.userService.getUser()
      .valueChanges()
      .pipe(
        takeUntil(this.ngUnsubscribe),
        tap(user => this.user = user),
        tap(user => this.refreshNextAvailableDeliveryDate(user))
      )
      .subscribe();
  }

  async refreshOrders(user: firebase.User) {
    const loader = await this.service.showLoading();
    this.orderService.getCurrentOrders(user.uid)
      .snapshotChanges()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe((data: any) => {
      this.orders = [];
      data.forEach(item => {
        const temp = item.payload.doc.data();
        temp['id'] = item.payload.doc.id;
        this.orders.push(temp);
      });
      loader.dismiss();
    }, error => {
      loader.dismiss();
      this.service.showToaster('Somthing went wrong', 4000);
    });
  }

  private refreshNextAvailableDeliveryDate(user: User) {
    const zipCode = get(user, 'deliveryAddress.zip', null);
    if (zipCode) {
      this.deliveryDateService.getNextAvailableDeliveryDate(zipCode)
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe(dates => {
        if (dates && dates.length) {
          this.nextAvailableDeliveryDate = dates[0];
          this.spotsLeftForNextAvailableDate = this.deliveryDateService
            .getSpotsRemainingToShow(this.nextAvailableDeliveryDate.ordersRemaining);
        } else {
          this.spotsLeftForNextAvailableDate = null;
        }
      });
    }
  }

  // async getCategories() {
  //   let loader = await this.service.showLoading();
  //   this.homeService.getCategories().snapshotChanges().subscribe(data => {
  //     this.categories = [];
  //     data.forEach(item => {
  //       let temp = item.payload.doc.data();
  //       temp["_id"] = item.payload.doc.id;
  //       this.categories.push(temp);
  //     });
  //     loader.dismiss();
  //   }, error => {
  //     loader.dismiss();
  //     this.service.showToaster("Somthing went wrong", 4000)
  //   })
  // }



  navigateToOrder(order: Order) {
    this.orderFlowService.updateOrder(order);
    this.navCtrl.navigateForward('/checkout/confirmation');
  }

  startNewOrder() {
    this.orderFlowService.startNewOrder({
      zip: this.user.deliveryAddress.zip,
      deliveryAddress: this.user.deliveryAddress,
      email: this.user.email,
      firstName: this.user.firstName,
      lastName: this.user.lastName,
      paymentInfo: this.user.paymentInfo
    });
    this.navCtrl.navigateForward('plan-selection');
  }

  goToCartPage() {
    this.navCtrl.navigateForward('/cart');
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
