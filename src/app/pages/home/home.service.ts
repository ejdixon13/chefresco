import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class HomeService {
  constructor(private afDB: AngularFirestore) { }

  getCategories() {
    return this.afDB.collection('categories');
  }

}
