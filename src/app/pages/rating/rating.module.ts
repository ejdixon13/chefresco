
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { RatingPage } from './rating.page';
import { TranslaterCustomModule } from './../../../app/translate.module';
import { RatingModule } from 'ngx-rating';

const routes: Routes = [
  {
    path: '',
    component: RatingPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RatingModule,
    TranslaterCustomModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RatingPage]
})
export class RatingPageModule { }
