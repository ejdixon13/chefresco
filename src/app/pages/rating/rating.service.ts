import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable()
export class RatingService {

  constructor(private afFS: AngularFirestore, private afAuth: AngularFireAuth) {
  }

  getAllReviews(itemId) {
    return this.afFS.collection('/menuItems/' + itemId + '/reviews');
  }

  saveReviewRatingData(itemId, postion, review) {
    return this.afFS.doc('/menuItems/' + itemId + '/reviews/' + postion).update(review);
  }
  updateOrderWithReviewData(orderId, index, review) {
    return this.afFS.doc('/orders/' + orderId + '/cart/' + index + '/item/' + '/review').update(review);
  }
}