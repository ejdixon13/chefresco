import { Service } from './../../services/app.service';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RatingService } from './rating.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.page.html',
  styleUrls: ['./rating.page.scss'],
  providers: [RatingService],
})
export class RatingPage {

  public review: any = {
    userId: '',
    rating: '',
    comment: ''
  }
  public isSaved: boolean = false;
  public menuItemIndex: number;
  public allReviewRating: any[] = [];
  public orderId: any;
  public menuItemId: any;
  constructor(
    private ratingService: RatingService,
    private route: ActivatedRoute,
    private navCtrl: NavController,
    private service: Service) {
    this.route.params.subscribe((id) => {
      this.menuItemId = id.productId,
        this.orderId = id.orderId,
        this.menuItemIndex = id.index
    });
    this.review.userId = localStorage.getItem('uid');
    this.getAllReviewAndRatingOfMenuItem(this.menuItemId)
  }

  getAllReviewAndRatingOfMenuItem(id) {
    this.ratingService.getAllReviews(id).valueChanges().subscribe((res: any) => {
      this.allReviewRating = res;
    })
  }


  async onSubmit() {
    let loader = await this.service.showLoading();
    this.ratingService.saveReviewRatingData(this.menuItemId, this.allReviewRating.length, this.review).then(() => {
      this.ratingService.updateOrderWithReviewData(this.orderId, this.menuItemIndex, this.review).then(() => {
        loader.dismiss();
      }).catch(error => {
        loader.dismiss();
        this.service.showToaster(error);
      })
    }).catch(error => {
      loader.dismiss();
      this.service.showToaster(error);
    })
  }

}
