import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { OrderDetailsPage } from './order-details.page';
import { TranslaterCustomModule } from './../../../app/translate.module';
import { RatingModule } from 'ngx-rating';

const routes: Routes = [
  {
    path: '',
    component: OrderDetailsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RatingModule,
    TranslaterCustomModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OrderDetailsPage],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class OrderDetailsPageModule { }
