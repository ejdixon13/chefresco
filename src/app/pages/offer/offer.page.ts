import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
import { OfferService } from './offer.service';
import { Service } from 'src/app/services/app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.page.html',
  styleUrls: ['./offer.page.scss'],
  providers: [OfferService]

})
export class OfferPage implements OnInit {

  public noOfItems: number;

  public offerProducts;
  public currency;

  constructor(

    private offerService: OfferService,
    private service: Service,
    private router: Router
  ) {
    this.currency = localStorage.getItem('currency');
    const cartItems = JSON.parse(localStorage.getItem('cartItem'));
    if (cartItems != null) { this.noOfItems = cartItems.length; }
  }

  async ngOnInit() {
    const loader = await this.service.showLoading();
    this.offerService.getMenuItems().snapshotChanges().
      subscribe((data: any) => {
        this.offerProducts = [];
        data.forEach(item => {
          const temp = item.payload.doc.data();
          temp['_id'] = item.payload.doc.id;
          this.offerProducts.push(temp);
        });
        loader.dismiss();
      }, error => {
        loader.dismiss();
        this.service.showToaster('Somthing went wrong', 4000);
      });
  }


  buyNow(productId) {
    localStorage.setItem('isFromOffer', 'true');
    this.router.navigate(['/product-detail', { productId: productId }]);
  }

}
