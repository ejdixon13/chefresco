import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable()
export class OfferService {

  constructor(private afFS: AngularFirestore) {
  }

  getMenuItems() {
    return this.afFS.collection('/menuItems', ref => ref.where('offer', '==', true));
  }

}
