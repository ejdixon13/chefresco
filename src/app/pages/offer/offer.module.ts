import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { OfferPage } from './offer.page';
import { TranslaterCustomModule } from './../../../app/translate.module';

const routes: Routes = [
  {
    path: '',
    component: OfferPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslaterCustomModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OfferPage]
})
export class OfferPageModule { }
