import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable()
export class FavouriteService {

  constructor(private afFS: AngularFirestore, private afAuth: AngularFireAuth) { }

  getFavourites() {
    return this.afFS.collection('/users/' + localStorage.getItem('uid') + '/favourite');
  }

  removeFromFavourite(key) {
    return this.afFS.doc('/users/' + localStorage.getItem('uid') + '/favourite/' + key)
      .delete();
  }


}
