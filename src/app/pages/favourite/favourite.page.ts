import { Service } from './../../services/app.service';
import { Component } from '@angular/core';
import { FavouriteService } from './favourite.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.page.html',
  styleUrls: ['./favourite.page.scss'],
  providers: [FavouriteService]
})
export class FavouritePage {
  public favouriteList;
  public noOfItems: number;

  constructor(
    public service: Service,
    private navCtrl: NavController,
    public favouriteService: FavouriteService
  ) {
    let cartItems = JSON.parse(localStorage.getItem('cartItem'));
    if (cartItems != null) this.noOfItems = cartItems.length;
    this.getWishlist();
  }

  navigateTo(productId) {
    this.navCtrl.navigateForward('/product-detail;productId=' + productId);
  }

  async removeFromFavourites(productId) {
    let loader = await this.service.showLoading();
    this.favouriteService.removeFromFavourite(productId)
      .then(response => {
        loader.dismiss();
        this.service.showToaster('Removed from favourite List');
      }).catch(error => {
        loader.dismiss();
        this.service.showToaster('Somthing went wrong to unfavourite');
      })

  }

  async getWishlist() {
    if (localStorage.getItem('uid')) {
      const loader = await this.service.showLoading();
      this.favouriteService.getFavourites().snapshotChanges().subscribe((data: any) => {
        this.favouriteList = [];
        data.forEach(item => {
          const temp = item.payload.doc.data();
          temp['_id'] = item.payload.doc.id;
          this.favouriteList.push(temp);
        });
        console.log('fav product list' + JSON.stringify(this.favouriteList))
        loader.dismiss();
      }, error => {
        loader.dismiss();
        this.service.showToaster('Somthing went wrong', 4000);
      })
    }
  }

}
