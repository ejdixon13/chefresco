import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore, DocumentChangeAction } from '@angular/fire/firestore';
import { OrderFlowService } from 'src/app/services/order-flow.service';
import { map } from 'rxjs/operators';
import { filter, get } from 'lodash';
 

@Injectable({
  providedIn: 'root'
})

export class ProductListService {
  constructor(
    private afFS: AngularFirestore,
    private orderFlowService: OrderFlowService,
  ) { }

  getMenuItems(categoryId: string) {
    const order = this.orderFlowService.getOrder();
    const deliveryDateStr = order.deliveryDate.format('YYYY-MM-DD');
    const filterFn = categoryId ? ref => ref.where('category', '==', categoryId) : ref => ref;
    return this.afFS.collection('menuItems', filterFn)
      .snapshotChanges()
      .pipe(
        map(data => this.filterByStartEndDates(data, deliveryDateStr))
      );
  }

  private filterByStartEndDates(data: DocumentChangeAction<any>[], deliveryDate: string) {
    return filter(data, item => {
      const menuItem = item.payload.doc.data();
      const endDate = get(menuItem, 'endDate', '2099-01-01');
      const startDate = get(menuItem, 'startDate', '2000-01-01');
      return startDate <= deliveryDate && deliveryDate < endDate;
    });
  }

}
