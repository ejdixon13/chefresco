import { Service } from './../../services/app.service';
import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductListService } from './product-list.service';
import { NavController, ToastController } from '@ionic/angular';
import { OrderFlowService } from '../../services/order-flow.service';
import { some, find, filter, get, map as _map } from 'lodash';
import { CategoryService } from '../category/category.service';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.page.html',
  styleUrls: ['./product-list.page.scss'],
  providers: [ProductListService]
})
export class ProductListPage implements OnDestroy {
  public menuItems;
  public itemsCopy = [];
  public selectedCategory;
  public categories;
  public noOfItems: number;
  public currency;
  private categorySubscription;

  constructor(
    private navCtrl: NavController,
    public productListService: ProductListService,
    public service: Service,
    public activatedRoute: ActivatedRoute,
    private orderFlowService: OrderFlowService,
    private categoryService: CategoryService,
    public toastController: ToastController
  ) {
    if (localStorage.getItem('isFromOffer') != null) {
      localStorage.removeItem('isFromOffer');
    }
    // this.activatedRoute.params.subscribe(item => {
    //   this.menuId = item.menuId;
    // });
    this.currency = localStorage.getItem('currency');
    this.categorySubscription = this.categoryService.getCategories()
      .snapshotChanges()
      .pipe(
        map(data => _map(data, item => ({
          key: item.payload.doc.id,
          value: item.payload.doc.data().title,
          color: item.payload.doc.data().color
        }))
        )
    ).subscribe(categories => this.categories = categories);
    this.selectedCategory = this.orderFlowService.getSelectedCategory();
    this.getMeals();
  }

  ionViewWillEnter() {
    const cart = JSON.parse(localStorage.getItem('cartItem'));
    if (cart != null) { this.noOfItems = cart.length; }
    console.log(this.noOfItems);
  }

  async getMeals(selectedCategory?: string) {
    const loader = await this.service.showLoading();
    this.productListService.getMenuItems(this.selectedCategory).subscribe((data: any) => {
      this.orderFlowService.setSelectedCategory(selectedCategory);
      this.menuItems = [];
      data.forEach(item => {
        const temp = item.payload.doc.data();
        temp['_id'] = item.payload.doc.id;
        this.menuItems.push(temp);
      });
      this.itemsCopy = this.menuItems;
      console.log('response of product list' + JSON.stringify(this.menuItems));
      loader.dismiss();
    }, error => {
      loader.dismiss();
      this.service.showToaster('Somthing went wrong', 4000);
    });
  }

  seacrchProduct(event) {
    this.itemsCopy = this.menuItems;
    const val = event.target.value;
    if (val && val.trim() != '') {
      this.itemsCopy = this.itemsCopy.filter((data) => {
        return (data.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      });
    }
  }

  getSpecialRequestForSelectedMeal(mealId: string) {
    const meal = this.orderFlowService.getMeal(mealId);
    if (meal && meal.specialRequests !== '') {
      return meal.specialRequests;
    } else {
      return null;
    }
  }

  getCategoryColor(categoryId: string) {
    const category = find(this.categories, cat => cat.key === categoryId);
    return category ? category.color : '';
  }

  getCategoryName(categoryId: string) {
    const category = find(this.categories, cat => cat.key === categoryId);
    return category ? category.value : '';
  }

  isMenuItemSelected(mealId: string) {
    return some(this.orderFlowService.getOrder().meals, meal => meal.mealId === mealId);
  }

  getMaxNumMeals() {
    return this.orderFlowService.getOrder().mealPlan.numMealsPerWeek;
  }

  getAllMealsQuantity() {
    return this.orderFlowService.getAllMealsQuantity();
  }

  getSelectedItemQuantity(mealId: string) {
    const meal = this.orderFlowService.getMeal(mealId);
    return meal ? meal.quantity : 0;
  }

  getCustomizations(mealId: string) {
    const meal = this.orderFlowService.getMeal(mealId);
    return get(meal, 'customizations', ['Add chicken', 'Sauce on side', 'Spicy Mango Italian Dressing']);
  }

  removeMeal(mealId: string) {
    this.orderFlowService.removeMeal(mealId);
  }

  async presentMealLimitToast() {
    const toast = await this.toastController.create({
      message: 'You have reached your meal limit. To add more, please adjust your meal quantity for the week.',
      duration: 2000
    });
    toast.present();
  }

  addOneToQuantity(mealId: string) {
    if (this.getAllMealsQuantity() < this.getMaxNumMeals()) {
      this.orderFlowService.addOneQuantityToMeal(mealId);
    } else {
      this.presentMealLimitToast();
    }
  }

  subtractOneFromQuantity(mealId: string) {
    this.orderFlowService.subtractOneFromQuantity(mealId);
  }

  navigateToProductDetail(productId) {
    this.navCtrl.navigateForward('/product-detail;productId=' + productId);
  }

  removeCustomization(mealId: string, customizationName: string) {
    this.orderFlowService.removeCustomization(mealId, customizationName);
  }

  removeSpecialRequest(mealId: string) {
    this.orderFlowService.getMeal(mealId).specialRequests = '';
  }

  navigateToCheckout() {
    if (this.service.isLoggedIn()) {
      if (this.orderFlowService.getZipCodeChanged()) {
        this.navCtrl.navigateForward('/checkout/address');
      } else {
        this.navCtrl.navigateForward('/checkout/submit-order');
      }
    } else {
      this.navCtrl.navigateForward('/checkout/create-account');
    }
  }

  goToCartPage() {
    this.navCtrl.navigateForward('/cart');
  }

  ngOnDestroy() {
    this.categorySubscription.unsubscribe();
  }

}
