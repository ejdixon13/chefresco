import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ProductListPage } from './product-list.page';
import { TranslaterCustomModule } from './../../../app/translate.module';
import { RatingModule } from "ngx-rating";

const routes: Routes = [
  {
    path: '',
    component: ProductListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslaterCustomModule,
    RatingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProductListPage],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
export class ProductListPageModule { }
