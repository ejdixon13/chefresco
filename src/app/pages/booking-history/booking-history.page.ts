import { Service } from './../../services/app.service';
import { Component, OnInit } from '@angular/core';
import { BookingHistoryService } from './booking-history.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-booking-history',
  templateUrl: './booking-history.page.html',
  styleUrls: ['./booking-history.page.scss'],
  providers: [BookingHistoryService]
})
export class BookingHistoryPage implements OnInit {

  public bookings;
  noOfItems: number;

  constructor(public bookingHistoryService: BookingHistoryService,
    public service: Service, public navCtrl: NavController
  ) { }

  ionViewWillEnter() {
    const cart = JSON.parse(localStorage.getItem('cartItem'));
    if (cart != null) {
      this.noOfItems = cart.length;
    }
  }

  async ngOnInit() {
    let loader = await this.service.showLoading();

    this.bookingHistoryService.getBookingHistory().valueChanges().subscribe((res: any) => {
      this.bookings = res;
      loader.dismiss();
    }, error => {
      loader.dismiss();
      this.service.showToaster(error.message);
    });
  }

  goToCartPage() {
    this.navCtrl.navigateForward('/cart');
  }

}
