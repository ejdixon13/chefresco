import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
    providedIn: 'root'
})
export class BookingHistoryService {

    private userId: any;
    constructor(private afFS: AngularFirestore, private afAuth: AngularFireAuth) {
        this.userId = afAuth.auth.currentUser.uid;
    }

    getBookingHistory() {
        return this.afFS.collection('/table-bookings', ref => ref.where('userID', '==', this.userId));

    }



}
