import { Service } from 'src/app/services/app.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NewsService } from '../news/news.service';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.page.html',
  styleUrls: ['./news-detail.page.scss'],
  providers: [NewsService]
})
export class NewsDetailPage implements OnInit {
  public newsDetails;
  public newsId: string;

  constructor(public newsService: NewsService, private service: Service, private route: ActivatedRoute) {
    this.route.params.subscribe(item => {
      this.newsId = item.newsId;
      console.log(item)
    });
  }

  async ngOnInit() {
    let loader = await this.service.showLoading();
    this.newsService.getNewsDetail(this.newsId).valueChanges()
      .subscribe((response: any) => {
        console.log("news", response)
        this.newsDetails = response;
        loader.dismiss();
      }, error => {
        this.service.showToaster(error.error.message);
        loader.dismiss();
      });
  }

}
