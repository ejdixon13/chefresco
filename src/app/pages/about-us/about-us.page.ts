import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.page.html',
  styleUrls: ['./about-us.page.scss'],
  providers: [CallNumber, EmailComposer]
})
export class AboutUsPage {
  public slideOpts = {
    effect: 'flip'
  };
  public contactNo: string = '7376421282';
  public description: string = "Pietech Solution is a global information technology, consulting and outsourcing company was founded by a highly motivated groups of marketing Team, software Developer.Talented, driven and principled people who are passionate about IT services, came together because they wanted to do something special. Special work for our clients is the front line, revolutionizing the way the industry works is a gradual effect, improving Industry through services is our long term goal.We offer a variety of services in the area of Mobile Application Development, Software Development ,Web Application development, CRM Business application, Responsive websites Design and Online Marketing. Our field service management software connects you with your field workforce, giving you complete visibility of the service delivery operation and driving efficiency and profitability.";

  constructor(private navCtrl: NavController, private callNumber: CallNumber, private emailComposer: EmailComposer) { }

  callUs() {
    this.callNumber.callNumber(this.contactNo, true)
      .then(() => console.log('Launche!'))
      .catch(() => console.log('Error launching dialer'));
  }

  navigateTo() {
    this.navCtrl.navigateForward('/location');
  }

  contactUs() {
    let email = {
      to: 'ionicfirebaseapp@gmail.com',
      isHtml: true
    };
    this.emailComposer.open(email);
  }

}
