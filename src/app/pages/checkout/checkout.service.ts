import { ConstService } from './../../services/constant.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { Order } from 'src/app/services/order-flow.service';
import { HTTP } from '@ionic-native/http/ngx';
import { APP_IS_ON_DEVICE } from 'src/app/shared/constant';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class CheckoutService {
  constructor(public http: HttpClient, public yesHttp: HTTP, public constService: ConstService, private afFS: AngularFirestore) { }

  placeOrderData(body) {
    return this.afFS.collection('/orders').add(body);
  }

  submitOrder(order: Order) {
    const formattedOrder = {
      ...order,
      deliveryDate: order.deliveryDate.format('YYYY-MM-DD')
    };
    if (APP_IS_ON_DEVICE) {
      this.yesHttp.setDataSerializer('json');
      return from(this.yesHttp.post(environment.azureApiEndpointSubmitOrder, formattedOrder, {}))
        .pipe(map(res => JSON.parse(res.data) ));
    } else {
      return this.http.post(environment.azureApiEndpointSubmitOrder, formattedOrder);
    }
  }

  chargeStripe(token, currency, amount, stripe_secret_key) {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Authorization', 'Bearer ' + stripe_secret_key);
    var params = new HttpParams()
      .set('currency', currency)
      .set('amount', amount)
      .set('description', 'description')
      .set('source', token);
    return new Promise(resolve => {
      this.http.post('https://api.stripe.com/v1/charges', params, {
        headers: headers
      }).subscribe(data => {
        resolve(data);
      });
    });
  }

  savePaymentDetails(orderId, paymentDetails) {
    const body = {
      payment: paymentDetails,
      paymentStatus: 'success'
    };
    return this.afFS.doc('/orders' + orderId).update(body);
  }

  saveLoyaltyPoints(userId, loyaltyData) {
    return this.afFS.collection('users/' + userId + '/loyaltyPoints').add(loyaltyData);
  }

  saveLoyaltyInOrderPoints(orderId, loyaltyData) {
    return this.afFS.collection('/orders' + orderId + '/loyaltyPoints').add(loyaltyData);
  }

  // save stripe card detail
  saveStripeCardDetail(body) {
    const authtoken = localStorage.getItem('token');
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', authtoken);
    return this.http.post(this.constService.base_url + 'api/users/stripe/card/info', body, {
      headers: headers
    });
  }

  stripePayment(body) {
    const authtoken = localStorage.getItem('token');
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', authtoken);
    return this.http.post(this.constService.base_url + 'api/users/stripe/payment', body, {
      headers: headers
    });
  }

  // stripe payemnt By saved card 
  stripePaymentBySavedCard(body) {
    const authtoken = localStorage.getItem('token');
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', authtoken);
    return this.http.post(this.constService.base_url + 'api/users/savedcard/stripe/payment', body, {
      headers: headers
    });
  }

  deleteSavedCard(body) {
    const authtoken = localStorage.getItem('token');
    const headers = new HttpHeaders().set('Content-Type', 'application/json').set('Authorization', authtoken);
    return this.http.post(this.constService.base_url + 'api/users/savedcard/delete/', body, {
      headers: headers
    });
  }

  getUserCardList(userId) {
    const headers = new HttpHeaders();
    return this.http.get(this.constService.base_url + 'api/users/card/info/data/' + userId, {
      headers: headers
    });
  }

}
