import { environment } from './../../../environments/environment';
import { Service } from './../../services/app.service';
import { Component, OnInit } from '@angular/core';
import { CheckoutService } from './checkout.service';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';
import { Stripe } from '@ionic-native/stripe/ngx';
import { NavController } from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';



@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
  providers: [CheckoutService, PayPal, Stripe]
})
export class CheckoutPage implements OnInit {
  public payPalEnvironmentSandbox = environment.payPalEnvironmentSandbox;
  public publishableKey = environment.stripePublishableKey;
  public stripe_secret_key = environment.stripeSecretKey;
  public orderDetails: any = {};
  public orderInfo;
  public currency;
  public paymentDetails: any = {
    paymentStatus: true
  };
  public stripeCardList: any[] = [];
  public cardInfo: any = {};
  public userId: string;
  public cvc: number;
  public isSaveCard = false;
  public newCardSelect = false;
  public selectedCradIndex: any;
  public isCardSelected = false;

  public paymentTypes: any = [
    { 'default': true, 'type': 'PayPal', 'value': 'paypal', 'logo': 'assets/img/paypal_logo.jpg' },
    { 'default': false, 'type': 'Stripe', 'value': 'stripe', 'logo': 'assets/img/stripe.png' },
    { 'default': false, 'type': 'COD', 'value': 'cod', 'logo': '' }
  ];


  constructor(
    public navCtrl: NavController,
    private service: Service,
    public checkoutService: CheckoutService,
    public payPal: PayPal,
    public stripe: Stripe,
    public afAuth: AngularFireAuth
  ) {
    this.currency = localStorage.getItem('currency');
    if (localStorage.getItem('orderInfo')) { this.orderInfo = JSON.parse(atob(localStorage.getItem("orderInfo"))); }
    this.orderInfo.cart = JSON.parse(localStorage.getItem('cartItem'));
    console.log('Order Info data' + JSON.stringify(this.orderInfo));
  }

  ngOnInit() {
    this.orderInfo.paymentOption = 'PayPal';
    this.orderInfo.orderId = Math.floor(Math.random() * 90000) + 10000;
    this.orderInfo.createdAt = Date.now();
    this.orderInfo.paymentStatus = 'pending';
    this.orderInfo.orderView = true;
    this.orderInfo.statusReading = [
      {
        title: 'Your order has been accepted.You will get notified the status here.',
        time: Date.now()
      }
    ];
    this.service.getUserDetails(this.afAuth.auth.currentUser.uid).valueChanges().subscribe((res: any) => {
      this.userId = this.afAuth.auth.currentUser.uid;
      this.orderInfo.userId = this.userId;
      this.orderInfo.userDetails = {
        email: res.email,
        name: res.name,
        mobileNo: res.mobileNo || '',
        userid: this.userId
      };
    });
  }

  choosePaymentType(paymentType) {
    this.orderInfo.paymentOption = paymentType;
    this.orderInfo.paymentType = paymentType;
    this.paymentDetails.paymentType = paymentType;
  }

  async checkout() {
    const loader = await this.service.showLoading();
    if (this.orderInfo.paymentOption == 'PayPal') {
      const config = {
        PayPalEnvironmentProduction: '',
        PayPalEnvironmentSandbox: this.payPalEnvironmentSandbox
      };
      this.checkoutService.placeOrderData(this.orderInfo).then((order: any) => {
        this.payPal.init(config).then(() => {
          this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({})).then(() => {
            const payment = new PayPalPayment(this.orderInfo.grandTotal, 'USD', 'Description', 'sale');
            this.payPal.renderSinglePaymentUI(payment).then((success: any) => {
              this.paymentDetails.transactionId = success.response.id;
              this.savePaymentData(order.key, this.paymentDetails, loader);
            }, (error) => {
              loader.dismiss();
              this.service.showToaster(error.message);
            });
          }, (error) => {
            loader.dismiss();
            this.service.showToaster(error.message);
          });
        }, (error) => {
          loader.dismiss();
          this.service.showToaster(error.message);
        });
      });
    } else if (this.orderInfo.paymentOption == 'Stripe') {
      if (this.orderInfo.grandTotal >= 50) {
        this.checkoutService.placeOrderData(this.orderInfo).then((order: any) => {

          let card: any = {
            number: this.cardInfo.cardNumber,
            expMonth: this.cardInfo.expiryMonth,
            year: this.cardInfo.expiryYear,
            cvc: this.cardInfo.cvc
          };

          this.stripe.createCardToken(card).then(token => {
            const stripe_token: any = token;
            // console.log('token', stripe_token);
            if (token) {
              this.checkoutService.chargeStripe(
                stripe_token.id,
                'USD',
                Math.round(this.orderInfo.grandTotal),
                this.stripe_secret_key
              ).then((result: any) => {

                // console.log('charge stripe response--', result);
                this.paymentDetails.transactionId = result.balance_transaction;
                card = {};
                this.saveLoyaltyData(order.key);
                const body: any = {
                  paymentDetails: this.paymentDetails,
                  paymentStatus: 'success'
                };
                this.checkoutService.savePaymentDetails(order.key, body)
                  .then(() => {
                    loader.dismiss();
                  });
              }, error => {

                // console.log('charge stripe error', error);
                loader.dismiss();
              });
            }
          }).catch(error => {
            loader.dismiss();

          });
          // this.checkoutService.saveStripeCardDetail(card).subscribe(res => {
          //   let pay = {
          //     userId: this.userId,
          //     amount: Math.round(this.orderInfo.grandTotal)
          //   }
          //   this.checkoutService.stripePayment(pay).subscribe((res: any) => {
          //     this.paymentDetails.transactionId = res.transactionId;// transaction id
          //     this.savePaymentData(order._id, this.paymentDetails, loader);
          //   }, error => {
          //     loader.dismiss();
          //     this.service.showToaster(error.message);
          //   });
          // });

        }, error => {
          loader.dismiss();
          this.service.showToaster(error.message);
        });
      } else {
        loader.dismiss();
        this.service.showAlert('Amount should be greater than $50.');
      }
    } else {
      this.placeOrder(loader);
    }

  }

  async placeOrder(loader) {

    this.checkoutService.placeOrderData(this.orderInfo).then((order: any) => {
      this.saveLoyaltyData(order.key);
      localStorage.removeItem('cartItem');
      loader.dismiss();
      this.navCtrl.navigateRoot('/thankyou');
    }, error => {
      loader.dismiss();
      this.service.showToaster(error.message);
    });
  }

  savePaymentData(orderId, paymentDetails, loader) {
    this.checkoutService.savePaymentDetails(orderId, paymentDetails).then(response => {
      this.saveLoyaltyData(orderId);
      localStorage.removeItem('cartItem');
      loader.dismiss();
      this.navCtrl.navigateRoot('/thankyou');
    }, error => {
      loader.dismiss();
      this.service.showToaster(error.message);
    });
  }

  onSaveCard() {
    this.isSaveCard = !this.isSaveCard;
  }

  saveLoyaltyData(orderId) {
    if (this.orderInfo.appliedLoyalty) {
      const loyaltyData = {
        credit: false,
        points: this.orderInfo.loyaltyPoints,
        orderId: orderId,
        dateAndTime: Date.now()
      };
      this.checkoutService.saveLoyaltyInOrderPoints(orderId, loyaltyData);
      this.checkoutService.saveLoyaltyPoints(this.userId, loyaltyData)
        .then(result => {
          this.service.showToaster('Loyalty Points updated');
        }, error => {
          this.service.showToaster(error.message);
        });
    }
  }

}
