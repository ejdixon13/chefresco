import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { OrderSection } from './order-summary-view';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrderFlowService } from 'src/app/services/order-flow.service';
import { get, omit } from 'lodash';
import { Service } from 'src/app/services/app.service';
import { FOOD_CATEGORY, APP_IS_ON_DEVICE } from 'src/app/shared/constant';
import { environment } from 'src/environments/environment';
import { CheckoutService } from './checkout.service';
import { catchError, tap, map } from 'rxjs/operators';
import { LoginService } from '../login/login.service';
import { Observable, Subscribable, Subscription } from 'rxjs';

declare var Stripe: any;

@Component({
    selector: 'app-order-summary-payment',
    templateUrl: 'order-summary-payment.page.html',
    styleUrls: ['./order-summary-payment.page.scss']
})

export class OrderSummaryPaymentPage implements OnInit {
    HIDDEN_SECTIONS = [OrderSection.PAYMENT_INFO];
    FOOD_CATEGORY = FOOD_CATEGORY;
    grandTotal: number;

    private readonly style = {
        base: {
          color: '#32325d',
          fontFamily: '"Montserrat", Helvetica, sans-serif',
          fontSmoothing: 'antialiased',
          fontSize: '18px',
          '::placeholder': {
              color: '#aab7c4',
              fontSize: '18px'
          }
        },
        invalid: {
          color: '#fa755a',
          iconColor: '#fa755a'
        }
      };
    constructor(
        private navCtrl: NavController,
        private fb: FormBuilder,
        public appService: Service,
        private loginService: LoginService,
        private checkoutService: CheckoutService,
        private orderFlowService: OrderFlowService,
        private alertController: AlertController
    ) { }

    ngOnInit() {
        this.setupStripe();
        this.grandTotal = this.orderFlowService.getGrandTotal();
        if (!this.appService.isLoggedIn()) {
           this.appService.getSettingsData()
                .get()
                .subscribe(data => {
                    const forcePickup = data.docs[0].data().forcePickup;
                    if (forcePickup) {
                        const forcePickupMsg = data.docs[0].data().forcePickupMsg;
                        this.presentForcePickupAlert(forcePickupMsg);
                    }
                });
        }
    }

    async presentForcePickupAlert(forcePickupMsg: any) {
        const alert = await this.alertController.create({
          header: forcePickupMsg.header,
          subHeader: forcePickupMsg.subHeader,
          message: forcePickupMsg.body,
          buttons: ['OK']
        });
        await alert.present();
      }

    async submitOrder(paymentMethod) {
        this.orderFlowService.updateOrder({
            paymentInfo: {
                paymentMethodId: paymentMethod.id,
                number: 'XXXX XXXX XXXX ' + paymentMethod.card.last4,
                cvc: 'XXXX',
                month: 'XX',
                year: paymentMethod.card.exp_year,
                billingZip: get(paymentMethod, 'billing_details.address.postal_code', '')
            }
        });
        if (this.appService.isLoggedIn()) {
            this.navCtrl.navigateForward('checkout/submit-order');
        } else {
            const loader = await this.appService.showLoading();
            this.checkoutService.submitOrder(this.orderFlowService.getOrder())
                .subscribe(
                    (res) => this.postSubmit(res),
                    (err) => {
                        if (err.status === 500) {
                            this.appService.showToaster('Oops, something went wrong.');
                        } else {
                            const error = APP_IS_ON_DEVICE ? JSON.parse(err.error) : err.error;
                            const msg = error.raw ? error.raw.message : error.message;
                            this.appService.showToaster(msg);
                        }
                        console.log(err);
                        loader.dismiss();
                    },
                    () => loader.dismiss()
                );
        }
    }

    private async postSubmit(res) {
        if (res.confirmationId) {
            this.orderFlowService.updateOrder({
                id: res.confirmationId
            });
            if (this.appService.isLoggedIn()) {
                this.navCtrl.navigateForward('checkout/submit-order');
            } else {
                const loader = await this.appService.showLoading();
                const order = this.orderFlowService.getOrder();
                this.loginService.login({
                    email: order.email,
                    password: order.password
                }).then((res: any) => {
                    localStorage.setItem('uid', res.user.uid);
                    loader.dismiss();
                    this.navCtrl.navigateForward('checkout/confirmation');
                  }).catch(error => {
                    this.appService.showToaster(error.message);
                    loader.dismiss();
                    console.log(error);
                  });
            }
        } else {
            this.appService.showToaster('Oops, something went wrong.');
        }
    }

    private setupStripe() {
        const stripe = Stripe(environment.stripePublishableKey);

        const elements = stripe.elements({
            fonts: [
                {
                  cssSrc: 'https://fonts.googleapis.com/css?family=Montserrat',
                },
              ]
        });
        const cardElement = elements.create('card', {style: this.style});
        cardElement.mount('#card-element');

        const form = document.getElementById('payment-form');

        const resultContainer = document.getElementById('payment-result');
        cardElement.addEventListener('change', function (event) {
            if (event.error) {
                resultContainer.textContent = event.error.message;
            } else {
                resultContainer.textContent = '';
            }
        });

        const handleServerResponse = async responseJson => {
            if (responseJson.error) {
                // An error happened when charging the card, show it in the payment form
                resultContainer.textContent = responseJson.error;
            } else {
                // Show a success message
                resultContainer.textContent = 'Success!';
            }
        };

        const handlePaymentMethodResult = async ({ paymentMethod, error }) => {
            if (error) {
                // An error happened when collecting card details, show error in payment form
                resultContainer.textContent = error.message;
            } else {
                this.submitOrder(paymentMethod);
                // Send paymentMethod.id to your server (see Step 3)
                // const response = await fetch('/pay', {
                //     method: 'POST',
                //     headers: { 'Content-Type': 'application/json' },
                //     body: JSON.stringify({ payment_method_id: paymentMethod.id })
                // });

                // const responseJson = await response.json();

                // handleServerResponse(responseJson);
            }
        };

        form.addEventListener('submit', async event => {
            event.preventDefault();
            resultContainer.textContent = '';
            const result = await stripe.createPaymentMethod({
                type: 'card',
                card: cardElement,
            });
            handlePaymentMethodResult(result);
        });
    }
}
