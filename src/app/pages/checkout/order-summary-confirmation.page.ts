import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { OrderFlowService } from 'src/app/services/order-flow.service';

@Component({
    selector: 'app-order-summary-confirmation',
    templateUrl: 'order-summary-confirmation.page.html',
    styleUrls: ['./order-summary-confirmation.page.scss']
})

export class OrderSummaryConfirmationPage implements OnInit {
    confirmationId: string;

    constructor(
        private navCtrl: NavController,
        private orderFlowService: OrderFlowService
    ) { }

    ngOnInit() {
        this.confirmationId = this.orderFlowService.getOrder().id;
     }

    navigateHome() {
        this.navCtrl.navigateForward('home');
    }
}
