import { Component, OnInit, Input } from '@angular/core';
import { OrderFlowService } from 'src/app/services/order-flow.service';
import { Order } from '../../services/order-flow.service';
import { get, includes } from 'lodash';
import { NavController } from '@ionic/angular';
import { FOOD_CATEGORY } from 'src/app/shared/constant';
import { SettingsService } from '../settings/settings.service';
import { getTwoDecimalNum } from 'src/app/shared/amount.util';
import { Observable } from 'rxjs';
import * as moment from 'moment';

export enum OrderSection {
    MEALS, ACCOUNT_INFO, DELIVERY_ADDRESS, PAYMENT_INFO
}

@Component({
    selector: 'app-order-summary-view',
    templateUrl: 'order-summary-view.html',
    styleUrls: ['./order-summary-view.scss']
})

export class OrderSummaryView implements OnInit {
    @Input() sectionsToHide: OrderSection[] = [];
    @Input() readonly: boolean;

    order: Order;
    maskedCardNumber: string;
    OrderSection = OrderSection;
    mealsPerWeekAmount: number;
    customizationsAmount: number;
    tax: number;
    deliveryFee: number;
    grandTotal: number;
    deliveryDateText: string;

    constructor(
        private navCtrl: NavController,
        private orderFlowService: OrderFlowService,
        private settingService: SettingsService
    ) { }

    ngOnInit() {
        this.order = this.orderFlowService.getOrder();
        this.deliveryDateText = this.getDeliveryDateText(this.order.deliveryDate);
        const cardNumber = get(this.order, 'paymentInfo.number', '');
        this.maskedCardNumber = 'XXXX XXXX XXXX ' + cardNumber.slice(cardNumber.length - 4);
        this.tax = this.orderFlowService.getTaxAmount();
        this.deliveryFee = this.orderFlowService.getDeliveryFee();
        this.mealsPerWeekAmount = this.orderFlowService.getMealsAmount();
        this.customizationsAmount = this.orderFlowService.getCustomizationsAmount();
        this.grandTotal = this.orderFlowService.getGrandTotal();
    }

    get pickupAddres() {
        return this.orderFlowService.getOrder().pickupAddress;
    }

    shouldHideSection(section: OrderSection) {
        return includes(this.sectionsToHide, section);
    }

    private getDeliveryDateText(deliveryDate: string | moment.Moment) {
        if ((deliveryDate as moment.Moment).format) {
            return (deliveryDate as moment.Moment).format('MM/DD/YYYY');
        } else {
            return moment(deliveryDate).format('MM/DD/YYYY');
        }
    }

    changeDeliveryAddress() {
        this.navCtrl.navigateBack('checkout/address');
    }

    changePaymentInfo() {
        this.navCtrl.navigateBack('checkout/payment');
    }

    changeMeals() {
        this.navCtrl.navigateBack('/product-list;menuId=' + FOOD_CATEGORY);
    }

}
