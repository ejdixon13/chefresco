import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrderFlowService } from 'src/app/services/order-flow.service';
import { OrderSection } from './order-summary-view';
import { FOOD_CATEGORY, APP_IS_ON_DEVICE } from 'src/app/shared/constant';
import { UserService } from 'src/app/services/user.service';
import { Service } from 'src/app/services/app.service';

@Component({
    selector: 'app-order-summary-create-account',
    templateUrl: 'order-summary-create-account.html'
})

export class OrderSummaryCreateAccountPage implements OnInit {
    HIDDEN_SECTIONS = [OrderSection.ACCOUNT_INFO, OrderSection.DELIVERY_ADDRESS, OrderSection.PAYMENT_INFO];
    accountCreation: FormGroup;
    passwordMismatch = false;
    FOOD_CATEGORY = FOOD_CATEGORY;
    constructor(
        private navCtrl: NavController,
        public fb: FormBuilder,
        private orderFlowService: OrderFlowService,
        private userService: UserService,
        private appService: Service
    ) { }

    ngOnInit() {
        const order = this.orderFlowService.getOrder();
        this.accountCreation = this.fb.group({
            email: [order.email || '', Validators.required], // auto fill the email-input-field.
            password: [order.password || '', Validators.compose([Validators.required, Validators.minLength(6)])], // auto fill the password-input-field.
            confirmPassword: [order.password || '', Validators.compose([Validators.required, Validators.minLength(6)])], // auto fill the password-input-field.
            firstName: [order.firstName || '', Validators.compose([Validators.required])], // auto fill the password-input-field.
            lastName: [order.lastName || '', Validators.compose([Validators.required])] // auto fill the password-input-field.
          });
    }

    checkPasswordMismatch() {
        const form = this.accountCreation.value;
        if (form.password && form.confirmPassword) {
            this.passwordMismatch = form.password !== form.confirmPassword;
        }
    }
    checkPasswordMismatchOnChange() {
        const form = this.accountCreation.value;
        if (this.passwordMismatch && form.password && form.confirmPassword) {
            this.passwordMismatch = form.password !== form.confirmPassword;
        }
    }


    next() {
        this.checkPasswordMismatch();
        if (!this.passwordMismatch) {
            const accountCreationFormValue = this.accountCreation.value;
            this.orderFlowService.updateOrder({
                email: accountCreationFormValue.email,
                password: accountCreationFormValue.password,
                firstName: accountCreationFormValue.firstName,
                lastName: accountCreationFormValue.lastName
            });
            if (!this.appService.isLoggedIn()) {
                this.userService.checkIfUserIsDuplicate(accountCreationFormValue.email)
                    .subscribe(
                        () => this.navCtrl.navigateForward('checkout/address'),
                        (err) => {
                            const error = APP_IS_ON_DEVICE ? JSON.parse(err.error) : err.error;
                            this.appService.showToaster(error.message);
                        }
                    );
            } else {
                this.navCtrl.navigateForward('checkout/address');
            }
        }
    }
}
