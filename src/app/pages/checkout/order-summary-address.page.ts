import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrderFlowService, Order } from 'src/app/services/order-flow.service';
import { OrderSection } from './order-summary-view';
import { Service } from './../../services/app.service';
import { get } from 'lodash';
import { Subscription } from 'rxjs';


@Component({
    selector: 'app-order-summary-address',
    templateUrl: 'order-summary-address.page.html'
})

export class OrderSummaryAddressPage implements OnInit, OnDestroy {
    HIDDEN_SECTIONS = [OrderSection.DELIVERY_ADDRESS, OrderSection.PAYMENT_INFO];
    deliveryAddresss: FormGroup;
    order: Order;
    settingsSubscription: Subscription;
    private forcePickup = false;
    constructor(
        private navCtrl: NavController,
        public fb: FormBuilder,
        public appService: Service,
        private orderFlowService: OrderFlowService
    ) { }

    ngOnInit() {
        this.order = this.orderFlowService.getOrder();
        this.deliveryAddresss = this.fb.group({
            line1: [get(this.order, 'deliveryAddress.line1', ''), Validators.required], // auto fill the email-input-field.
            line2: [get(this.order, 'deliveryAddress.line2', '')],
            city: [get(this.order, 'deliveryAddress.city', ''), Validators.required],
            state: ['AZ', Validators.compose([Validators.required])],
            zip: [this.order.zip || get(this.order, 'deliveryAddress.zip', ''), Validators.compose([Validators.required])]
        });
        this.settingsSubscription = this.appService.getSettingsData()
            .get()
            .subscribe(data => {
                this.forcePickup = data.docs[0].data().forcePickup;
            });
    }

    async next() {
        this.orderFlowService.updateOrder({
            deliveryAddress: this.deliveryAddresss.value,
            pickupAddress: null
        });
        if (this.appService.isLoggedIn()) {
            this.orderFlowService.setZipCodeChanged(false);
            this.navCtrl.navigateForward('checkout/submit-order');
        } else {
            if (this.forcePickup) {
                this.appService.getPickupLocationForZip(this.order.zip)
                    .get()
                    .subscribe(data => {
                        if (data.docs.length) {
                            this.orderFlowService.updateOrder({
                                pickupAddress: data.docs[0].data().address
                            });
                            this.navCtrl.navigateForward('checkout/payment');
                        } else {
                            this.appService.showToaster('We don\'t currently provide service to that address. Please contact us about pick up options.');
                        }
                    });
            } else {
                this.navCtrl.navigateForward('checkout/payment');
            }
        }
    }

    ngOnDestroy() {
        this.settingsSubscription.unsubscribe();
    }
}
