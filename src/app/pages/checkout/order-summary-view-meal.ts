import { Component, OnInit, Input } from '@angular/core';
import { Customization } from 'src/app/services/order-flow.service';
import { Meal } from '../../services/order-flow.service';

@Component({
    selector: 'app-order-summary-view-meal',
    templateUrl: 'order-summary-view-meal.html',
    styleUrls: ['./order-summary-view-meal.scss']
})

export class OrderSummaryViewMeal implements OnInit {

    @Input() meal: Meal;

    constructor(
    ) { }

    ngOnInit() { }
}
