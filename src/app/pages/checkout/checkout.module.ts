import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CheckoutPage } from './checkout.page';
import { TranslaterCustomModule } from './../../../app/translate.module';
import { OrderSummaryCreateAccountPage } from './order-summary-create-account.page';
import { OrderSummaryAddressPage } from './order-summary-address.page';
import { OrderSummaryPaymentPage } from './order-summary-payment.page';
import { OrderSummaryConfirmationPage } from './order-summary-confirmation.page';
import { OrderSummaryView } from './order-summary-view';
import { OrderSummaryViewMeal } from './order-summary-view-meal';
import { SharedModule } from 'src/app/shared/shared.module';
import { OrderSummarySubmitOrderPage } from './order-summary-submit-order.page';

const routes: Routes = [
  {
    path: '',
    component: CheckoutPage
  },
  {
    path: 'create-account',
    component: OrderSummaryCreateAccountPage
  },
  {
    path: 'address',
    component: OrderSummaryAddressPage
  },
  {
    path: 'payment',
    component: OrderSummaryPaymentPage
  },
  {
    path: 'submit-order',
    component: OrderSummarySubmitOrderPage
  },
  {
    path: 'confirmation',
    component: OrderSummaryConfirmationPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TranslaterCustomModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    CheckoutPage,
    OrderSummaryCreateAccountPage,
    OrderSummaryAddressPage,
    OrderSummaryConfirmationPage,
    OrderSummaryPaymentPage,
    OrderSummaryView,
    OrderSummaryViewMeal,
    OrderSummarySubmitOrderPage
  ]
})
export class CheckoutPageModule { }
