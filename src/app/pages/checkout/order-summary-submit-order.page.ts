import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { OrderFlowService } from 'src/app/services/order-flow.service';
import { Service } from 'src/app/services/app.service';
import { FOOD_CATEGORY, APP_IS_ON_DEVICE } from 'src/app/shared/constant';
import { CheckoutService } from './checkout.service';
import { Subscription } from 'rxjs';
import { filter, switchMap, tap } from 'rxjs/operators';


@Component({
    selector: 'app-order-summary-submit-order',
    templateUrl: 'order-summary-submit-order.page.html',
    styleUrls: ['./order-summary-submit-order.page.scss']
})

export class OrderSummarySubmitOrderPage implements OnInit, OnDestroy {
    FOOD_CATEGORY = FOOD_CATEGORY;
    settingsSubscription: Subscription;

    constructor(
        private navCtrl: NavController,
        private appService: Service,
        private orderFlowService: OrderFlowService,
        private checkoutService: CheckoutService,
        private alertController: AlertController
    ) { }

    ngOnInit() {
        const order = this.orderFlowService.getOrder();
        this.settingsSubscription = this.appService.getSettingsData()
            .get()
            .pipe(
                filter(data => data.docs[0].data().forcePickup),
                tap(data => this.presentForcePickupAlert(data.docs[0].data().forcePickupMsg)),
                switchMap(() => this.appService.getPickupLocationForZip(order.zip).get())
            )
            .subscribe(data => {
                if (data.docs.length) {
                    this.orderFlowService.updateOrder({
                        pickupAddress: data.docs[0].data().address
                    });
                } else {
                    this.appService.showToaster('We don\'t currently provide service to that address. Please contact us about pick up options.');
                }
            });
    }

    async presentForcePickupAlert(forcePickupMsg: any) {
        const alert = await this.alertController.create({
          header: forcePickupMsg.header,
          subHeader: forcePickupMsg.subHeader,
          message: forcePickupMsg.body,
          buttons: ['OK']
        });
        await alert.present();
      }

    async submitOrder() {
        const loader = await this.appService.showLoading();
        this.checkoutService.submitOrder(this.orderFlowService.getOrder())
            .subscribe(
                (res) => this.postSubmit(res),
                (err) => {
                    if (err.status === 500) {
                        this.appService.showToaster('Oops, something went wrong.');
                    } else {
                        const error = APP_IS_ON_DEVICE ? JSON.parse(err.error) : err.error;
                        const msg = error.raw ? error.raw.message : error.message;
                        this.appService.showToaster(msg);
                    }
                    console.log(err);
                    loader.dismiss();
                },
                () => loader.dismiss()
            );
    }

    private async postSubmit(res) {
        if (res.confirmationId) {
            this.orderFlowService.updateOrder({
                id: res.confirmationId
            });
            this.navCtrl.navigateForward('checkout/confirmation');
        } else {
            this.appService.showToaster('Oops, something went wrong.');
        }
    }

    navigateHome() {
        this.navCtrl.navigateForward('home');
    }

    ngOnDestroy() {
        this.settingsSubscription.unsubscribe();
    }
}
