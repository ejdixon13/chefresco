import { LoadingController, ToastController, AlertController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FOOD_CATEGORY } from '../shared/constant';

@Injectable({
  providedIn: 'root',
})
export class Service {
  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController,
    private afAuth: AngularFireAuth,
    private afFS: AngularFirestore) { }

  async showLoading(message?) {
    const loader = await this.loadingCtrl.create({
      // content: message || 'Please wait..'
    });
    loader.present();
    return loader;
  }

  async showToaster(message, duration?) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: duration || 3000
    });
    toast.present();
    return toast;
  }

  async showAlert(message) {
    const alert = await this.alertCtrl.create({
      header: 'Pincodes',
      message: message,
      buttons: ['OK']
    });
    await alert.present();
  }

  getSettingsData() {
    return this.afFS.collection('settings');
  }


  getPickupLocationForZip(zip: string) {
    return this.afFS.collection('pickup-options', ref => ref.where('zipCodeList', 'array-contains', zip));
  }

  getUserDetails(uid) {
    return this.afFS.collection('users').doc(uid);
  }

  isLoggedIn() {
      return localStorage.getItem('uid') != null;
  }


  insertAZZipCodes() {
    const cityZipCodeMap = { Aguila: [ '85320' ], 
    Anthem: [ '85086' ], 
    ApacheJunction: [ '85120', '85119' ], 
    ArizonaCity: [ '85123' ], 
    Arlington: [ '85322' ], 
    Avondale: [ '85392', '85323' ], 
    Buckeye: [ '85326', '85396' ], 
    Carefree: [ '85377' ], 
    CasaBlanca: [ '85121' ], 
    CasaGrande: [ '85194', '85122', '85193' ], 
    Chandler: [ '85226', '85248', '85224', '85225', '85286', '85249' ], 
    Coolidge: [ '85128' ], 
    ElMirage: [ '85335' ], 
    Eloy: [ '85131' ], 
    Florence: [ '85132' ], 
    FortMcDowell: [ '85264' ], 
    FountainHills: [ '85268' ], 
    GilaBend: [ '85337' ], 
    Gilbert: [ '85296', '85234', '85298', '85297', '85233', '85295' ], 
    Glendale:  
     [ '85308', 
       '85302', 
       '85303', 
       '85304', 
       '85309', 
       '85305', 
       '85306', 
       '85307', 
       '85301' ], 
    GoldCanyon: [ '85118' ], 
    Goodyear: [ '85338', '85395' ], 
    Kearny: [ '85137' ], 
    LitchfieldPark: [ '85340' ], 
    Mammoth: [ '85618' ], 
    Maricopa: [ '85138', '85139' ], 
    Mesa:  
     [ '85212', 
       '85201', 
       '85215', 
       '85210', 
       '85209', 
       '85208', 
       '85207', 
       '85206', 
       '85205', 
       '85204', 
       '85203', 
       '85213', 
       '85202' ], 
    Morristown: [ '85342' ], 
    NewRiver: [ '85087' ], 
    Oracle: [ '85623' ], 
    PaloVerde: [ '85343' ], 
    ParadiseValley: [ '85253' ], 
    Peoria: [ '85383', '85382', '85381', '85345' ], 
    Phoenix:  
     [ '85032', 
       '85006', 
       '85085', 
       '85083', 
       '85004', 
       '85054', 
       '85053', 
       '85051', 
       '85050', 
       '85048', 
       '85045', 
       '85044', 
       '85007', 
       '85012', 
       '85008', 
       '85254', 
       '85353', 
       '85009', 
       '85003', 
       '85013', 
       '85014', 
       '85339', 
       '85015', 
       '85016', 
       '85043', 
       '85017', 
       '85042', 
       '85331', 
       '85018', 
       '85019', 
       '85020', 
       '85041', 
       '85040', 
       '85037', 
       '85035', 
       '85034', 
       '85033', 
       '85021', 
       '85031', 
       '85029', 
       '85028', 
       '85027', 
       '85024', 
       '85023', 
       '85022', 
       '85310' ], 
    Picacho: [ '85141' ], 
    QueenCreek: [ '85142' ], 
    RedRock: [ '85145' ], 
    RioVerde: [ '85263' ], 
    Sacaton: [ '85147' ], 
    SanTanValley: [ '85143', '85140' ], 
    Scottsdale:  
     [ '85258', 
       '85266', 
       '85257', 
       '85256', 
       '85251', 
       '85255', 
       '85262', 
       '85260', 
       '85259', 
       '85250' ], 
    Stanfield: [ '85172' ], 
    SunCity: [ '85373', '85351' ], 
    SunCityWest: [ '85375' ], 
    Superior: [ '85173' ], 
    Surprise: [ '85388', '85374', '85379', '85387' ], 
    Tempe: [ '85284', '85281', '85282', '85283' ], 
    Tonopah: [ '85354' ], 
    Waddell: [ '85355' ], 
    Wickenburg: [ '85390' ], 
    Wittmann: [ '85361' ], 
    Youngtown: [ '85363' ] };
    const collection = this.afFS.collection('delivery-options');
    Object.keys(cityZipCodeMap).forEach(key => {
      collection.doc(key).set({
        deliveryDays: ['Saturday', 'Sunday'],
        zipCodeList: cityZipCodeMap[key]
      });
    });
  }

  updateMenuItemStartEndDates() {
    const collection = this.afFS.collection('menuItems', ref => ref.where('category', '==', FOOD_CATEGORY));
    return collection.ref.get().then(resp => {
      console.log(resp.docs)
      let batch = this.afFS.firestore.batch();
  
      resp.docs.forEach(menuItemRef => {
        batch.update(menuItemRef.ref, { 'startDate': '2020-03-08', 'endDate': '2020-05-15' });
      })
      batch.commit().catch(err => console.error(err));
    }).catch(error => console.error(error));
  }

}




