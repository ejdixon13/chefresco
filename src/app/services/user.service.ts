import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Address, PaymentInfo } from './order-flow.service';
import { HttpClient } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import { APP_IS_ON_DEVICE } from '../shared/constant';
import { from, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

export interface User {
    id: string;
    email: string;
    firstName: string;
    lastName: string;
    deliveryAddress: Address;
    paymentInfo: PaymentInfo;
    role: string;
    stripeUID: string;
}

@Injectable({providedIn: 'root'})
export class UserService {
    constructor(
        private afFS: AngularFirestore,
        private afAuth: AngularFireAuth,
        public http: HttpClient,
        public yesHttp: HTTP
    ) { }

    getUser() {
        return this.afFS.collection('users').doc<User>(this.afAuth.auth.currentUser.uid);
    }

    checkIfUserIsDuplicate(email: string): Observable<any> {
        const user = {
            email: email
        };

        if (APP_IS_ON_DEVICE) {
            this.yesHttp.setDataSerializer('json');
            return from(this.yesHttp.post(environment.azureApiEndpointDuplicateUser, user, {}))
              .pipe(map(res => JSON.parse(res.data) ));
          } else {
            return this.http.post(environment.azureApiEndpointDuplicateUser, user);
          }
    }
}
