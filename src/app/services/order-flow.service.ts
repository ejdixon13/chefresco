import { Injectable } from '@angular/core';
import { filter, find, each } from 'lodash';
import * as moment from 'moment';
import { getTwoDecimalNum } from '../shared/amount.util';
import { SettingsService } from '../pages/settings/settings.service';
import { map, distinctUntilChanged, delay } from 'rxjs/operators';
import { Observable } from 'rxjs';

export interface Customization {
    id: string;
    name: string;
    value: number;
}
export interface Meal {
    mealId: string;
    title: string;
    thumbnailUrl: string;
    quantity: number;
    customizations?: Customization[];
    specialRequests?: string;
}
export class Order {
    id?: string;
    confirmationId?: string;
    email: string;
    password?: string;
    firstName?: string;
    lastName?: string;
    deliveryAddress?: Address;
    pickupAddress?: PickupAddress;
    billingAddress?: Address;
    paymentInfo?: PaymentInfo;
    zip?: string;
    mealPlan?: PlanOption;
    deliveryDate?: moment.Moment;
    meals?: Meal[] = [];
}

export class Address {
    line1: string;
    line2: string;
    city: string;
    state: string = 'AZ';
    zip: string;
}

export class PickupAddress extends Address {
    name: string;
    pickupTime: string;
}

export interface PaymentInfo {
    paymentMethodId: string;
    number: string;
    month: string;
    year: string;
    cvc: string;
    billingZip: string;
}
export interface PlanOption {
    id: string;
    numMealsPerWeek: number;
    pricePerMeal?: number;
    color?: string;
}

const fakeOrder: Order = {
    id: 'fake',
    email: 'fake@fakemail.com',
    password: 'fakePassword!',
    firstName: 'Fake',
    lastName: 'Dixon',
    deliveryAddress: {
        line1: '2300 E Fake St',
        line2: 'Apt #34',
        city: 'Phoenix',
        state: 'AZ',
        zip: '85203'
    },
    paymentInfo: {
        paymentMethodId: 'TEST',
        number: '1234123412341234',
        month: '02',
        year: '2025',
        cvc: '1234',
        billingZip: '85203'
    },
    mealPlan: {
        id: '4x',
        numMealsPerWeek: 4,
        pricePerMeal: 12
    },
    deliveryDate: moment(),
    meals: [
        {
            mealId: '4e0d622822454728ddd510',
            thumbnailUrl: 'http://res.cloudinary.com/ujju/image/upload/c_scale,w_748/v1474264357/food5_z9iqbs.jpg',
            title: 'Enchiladas',
            quantity: 3,
            customizations: [
                {
                    id: 'addChicken',
                    name: 'Add Chicken',
                    value: 1
                },
                {
                    id: 'sauceOnSide',
                    name: 'Sauce on Side',
                    value: 2
                },
                {
                    id: 'lemonDrizzle',
                    name: 'Lemon Drizzle',
                    value: 3
                }
            ],
            specialRequests: 'Something Special'
        },
        {
            mealId: '4e0d6yb822454728ddd511',
            thumbnailUrl: 'http://res.cloudinary.com/ujju/image/upload/c_scale,w_748/v1474264357/food5_z9iqbs.jpg',
            title: 'Bison Meat Loaf',
            quantity: 1,
            customizations: [
            ],
            specialRequests: ''
        }
    ]
}

@Injectable({
    providedIn: 'root'
})
export class OrderFlowService {
    private order: Order = new Order();
    // private order = fakeOrder;
    private zipCodeChanged: boolean;
    private selectedCategory: string;
    private settings: any;

    constructor(
        private settingsService: SettingsService
    ) {
        this.settingsService.getSettings().get().pipe(
            map(settingsRef => settingsRef.data()),
            distinctUntilChanged()
        ).subscribe(settings => this.settings = settings);
    }

    clearOrder() {
        this.order = new Order();
    }

    startNewOrder(orderFieldsToUpdate: Partial<Order>) {
        this.order = new Order();
        this.updateOrder(orderFieldsToUpdate);
    }

    updateOrder(orderFieldsToUpdate: Partial<Order>) {
        this.order = Object.assign(this.order, orderFieldsToUpdate);
    }

    getOrder() {
        return this.order;
    }

    setZipCodeChanged(changed: boolean) {
        this.zipCodeChanged = changed;
    }

    setSelectedCategory(category: string) {
        this.selectedCategory = category;
    }

    getSelectedCategory() {
        return this.selectedCategory;
    }

    getZipCodeChanged() {
        return this.zipCodeChanged;
    }

    getMeal(mealId: string): Meal {
        return find(this.getOrder().meals, m => m.mealId === mealId);
    }

    addMeal(meal: Meal) {
        const meals = this.getOrder().meals;
        meals.push(meal);
        this.updateOrder({
            meals: meals
        });
    }

    removeMeal(mealId: string) {
        const meals = this.getOrder().meals;
        this.updateOrder({
          meals: filter(meals, meal => meal.mealId !== mealId)
        });
    }

    getAllMealsQuantity() {
        const meals = this.getOrder().meals || [];
        return meals
            .map(meal => meal.quantity)
            .reduce((quantity, nextQuantity) => quantity + nextQuantity, 0);
    }

    addOneQuantityToMeal(mealId: string) {
        const meal = this.getMeal(mealId);
        const numMeals = this.getOrder().mealPlan.numMealsPerWeek;
        if (meal && meal.quantity < numMeals) {
            ++meal.quantity;
        }
    }

    subtractOneFromQuantity(mealId: string) {
        const meal = this.getMeal(mealId);
        if (meal && meal.quantity > 1) {
            --meal.quantity;
        }
    }

    removeCustomization(mealId: string, customizationName: string) {
        const meal = this.getMeal(mealId);
        meal.customizations = filter(meal.customizations, c => c.name !== customizationName);
    }

    isMealAdded(mealId: string): boolean {
        return !!this.getMeal(mealId);
    }

    getTaxAmount(): number {
        return this.getTaxAmountWithTaxPercent(this.settings.taxPercent);
    }

    getDeliveryFee(): number {
        return this.settings.deliveryFee;
    }
    private getTaxAmountWithTaxPercent(taxPercent: number): number {
        return getTwoDecimalNum((this.getMealsAmount() + this.getCustomizationsAmount()) *
            getTwoDecimalNum(taxPercent / 100));
    }
    getMealsAmount(): number {
        const meals = this.order.meals;
        let numMeals = 0;
        each<Meal>(meals, meal => numMeals += meal.quantity);
        return getTwoDecimalNum(this.order.mealPlan.pricePerMeal * numMeals);
    }

    getCustomizationsAmount(): number {
        const customizationsAmount = this.order.meals.map(meal => {
            return meal.customizations.map(cust => cust.value || 0).reduce((prev, curr) => prev + curr, 0)
        }).reduce((prev, curr) => prev + curr, 0);
        return getTwoDecimalNum(customizationsAmount);
    }

    getGrandTotal(): number {
        const tax = this.getTaxAmountWithTaxPercent(this.settings.taxPercent);
        return this.getMealsAmount() + this.getCustomizationsAmount() + this.settings.deliveryFee + tax;
    }
}
