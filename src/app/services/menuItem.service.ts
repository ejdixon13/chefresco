import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({providedIn: 'root'})
export class MenuItemService {
    constructor(
        private afFS: AngularFirestore
    ) { }

    getFeaturedMenuItems() {
        return this.afFS.collection('menuItems', ref => ref.where('featured', '==', true));
    }
}
