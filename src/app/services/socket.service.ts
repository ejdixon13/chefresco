import { Service } from './app.service';
import { ConstService } from './constant.service';
import { Injectable, NgZone } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class SocketService {
    // private zone;
    // private clientInfo = {
    //     userId: ''
    // };

    // constructor(
    //     public constService: ConstService,
    //     public userService: Service,
    //     private socket: Socket
    // ) {
    // }

    // establishConnection() {
    //     this.socket = io(this.constService.base_url);
    //     this.zone = new NgZone({ enableLongStackTrace: false });
    //     this.socket.on('connect', function () {
    //         console.log('connected');
    //     });
    //     this.socket.on('disconnect', function () {
    //         console.log('disconnected');
    //     });
    //     this.socket.on('error', function (e) {
    //         console.log('System', e ? e : 'A unknown error occurred');
    //     });

    //     // Commented code from the beginning
    //     // this.userService.getUser().subscribe((Response: any) => {
    //     //     this.clientInfo.userId = Response._id;
    //     //     this.socket.emit('restaurantInfo', this.clientInfo);
    //     // }, error => {
    //     //     // console.error(error);
    //     //     localStorage.removeItem('token');
    //     // });
    // }

    // emitMessage(messageBody) {
    //     this.socket.emit('user_message', {
    //         message: messageBody.message,
    //         user_id: messageBody.sellerId
    //     });
    // }

    // getLastMessage() {
    //     // let observable = new Observable(observer => {
    //     //     this.socket.on('message' + this.clientInfo.userId, (data) => {
    //     //         observer.next(data);
    //     //     });
    //     // })
    //     // return observable;
    // }


}
