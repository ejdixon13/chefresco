import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})

export class ConstService {
    public base_url = environment.apiEndpoint;
}