import { FavouriteService } from './pages/favourite/favourite.service';
import { NewsService } from './pages/news/news.service';
import { environment } from './../environments/environment';
import { Service } from './services/app.service';
import { Component } from '@angular/core';
import { Platform, Events, NavController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { SocketService } from './services/socket.service';
// import { OneSignal } from '@ionic-native/onesignal/ngx';
// import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { OfferService } from './pages/offer/offer.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { SettingsService } from './pages/settings/settings.service';
import { OrderFlowService } from './services/order-flow.service';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Market } from '@ionic-native/market/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  providers: [NewsService, OfferService, FavouriteService]
})
export class AppComponent {

  constructor(
    public afDB: AngularFireDatabase,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public service: Service,
    public socketService: SocketService,
    public events: Events,
    private router: Router,
    private navCtrl: NavController,
    private offerService: OfferService,
    private newsService: NewsService,
    private favouriteService: FavouriteService,
    public translateService: TranslateService,
    private settingsService: SettingsService,
    private orderFlowService: OrderFlowService,
    private appVersion: AppVersion,
    private alertController: AlertController,
    private market: Market

  ) {
    this.initializeApp();
    this.getSettingInfo();
    this.setTranslateServiceAndPlatformDirection();
  }
  public noOfItems: number;
  public newsCounter: number;
  public userInfo = { imgUrl: 'assets/img/profile.jpg', name: 'Guest' };

  public routes = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home',
      loginRequired: false,
      count: 0
    },
    {
      title: 'Start a New Order',
      url: '/plan-selection',
      icon: 'apps',
      loginRequired: false,
      routeDirection: 'forward',
      count: 0
    },
    // {
    //   title: 'Previous Orders',
    //   url: '/offer',
    //   icon: 'pricetag',
    //   loginRequired: false,
    //   count: 0
    // },
    {
      title: 'Contact The Chef',
      url: '/contact',
      icon: 'call',
      loginRequired: false,
      count: 0
    },
    // {
    //   title: 'About Us',
    //   url: '/about-us',
    //   icon: 'contacts',
    //   loginRequired: false,
    //   count: 0
    // },
    // {
    //   title: 'Invite a Friend',
    //   url: '/book-table',
    //   icon: 'list-box',
    //   loginRequired: false,
    //   count: 0
    // },
    // {
    //   title: 'Settings',
    //   url: '/settings',
    //   icon: 'settings',
    //   loginRequired: false,
    //   count: 0
    // }
  ];

  private initializeApp() {
    // TODO: understand socket service and what it does
    // this.socketService.establishConnection();
    this.platform.ready().then((res) => {
      // this.service.updateMenuItemStartEndDates();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if (this.isLoggedin(true)) {
        this.router.navigate(['/home']);
      } else {
        this.router.navigate(['/sign-up']);
      }
      if (res == 'cordova') {
        // this.oneSignal.startInit(environment.onesignalAppId, environment.googleProjectNumber);
        // this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
        // this.oneSignal.getIds().then(response => {

        //   if (localStorage.getItem('uid') != null) {
        //     const uid = localStorage.getItem('uid');
        //     localStorage.setItem('playerId', response.userId);
        //     this.afDB.object('/users/' + uid).update({
        //       playerId: response.userId
        //     });
        //   }
        // });

        // this.oneSignal.handleNotificationReceived().subscribe(() => {
        // });
        // this.oneSignal.handleNotificationOpened().subscribe(() => {
        // });
        // this.oneSignal.endInit();
      }
    });
  }

  private getSettingInfo() {
    if (this.isLoggedin(true)) {
      this.service.getUserDetails(localStorage.getItem('uid')).valueChanges().subscribe((res: any) => {
        this.events.publish('profileInfo', { imageUrl: res.image, username: res.name });
        this.userInfo.imgUrl = res.image ? res.image : 'assets/img/profile.jpg';
        this.userInfo.name = res.firstName;

        // this.favouriteService.getFavourites().valueChanges().subscribe((res: any) => {
        //   this.routes.forEach((item: any) => {
        //     if (item.title === 'Favourite') {
        //       item.count = res.length;
        //     }
        //   });
        // });

      });
    }
    // this.newsService.getNews().valueChanges().subscribe((res: any) => {
    //   this.routes.forEach((item: any) => {
    //     if (item.title == 'News') item.count = res.length;
    //   });
    // });
    // this.offerService.getMenuItems().valueChanges().subscribe((res: any) => {
    //   this.routes.forEach((item: any) => {
    //     if (item.title === 'Offers') {
    //       item.count = res.length;
    //     }
    //   });
    // });
    this.events.subscribe('profileInfo', (info) => {
      this.userInfo.imgUrl = info.imageUrl ? info.imageUrl : 'assets/img/profile.jpg';
      this.userInfo.name = info.username;
    });
    this.settingsService.getSettings().get().subscribe((res: any) => {
      const settings = res.data();
      this.verifyVersion(settings);
      if (settings.currency && settings.currency.currencySymbol) {
        localStorage.setItem('currency', res.currency.currencySymbol);
      } else {
        localStorage.setItem('currency', '$');
      }
    });
  }

  private async verifyVersion(settings: any) {
    const installedVersion = await this.appVersion.getVersionNumber();
    if (settings.version > installedVersion) {
      await this.presentUpgradeAppAlert(settings);
    }
  }

  private async presentUpgradeAppAlert(settings: any) {
    const alert = await this.alertController.create({
      header: 'We have New Features!',
      message: 'In order to use these features (and for our app to work properly :) ), please update the app.',
      backdropDismiss: false,
      buttons: [
        {
          text: 'Update',
          handler: data => {
            if ((window as any).cordova) {
              if (this.platform.is('ios')) {
                this.market.open(settings.iosStoreAppId);
              } else if (this.platform.is('android')) {
                this.market.open(settings.playStoreAppId);
              }
            }
            return false;
          }
        }
      ]
    });
    await alert.present();
  }

  private setTranslateServiceAndPlatformDirection() {
    const value = localStorage.getItem('language');
    const language = value != null ? value : 'en';
    this.translateService.use(language);
    // language == 'ar' ? this.platform.setDir('rtl', true) : this.platform.setDir('ltr', true);;
  }

  isLoggedin(checkRequired) {
    if (checkRequired) {
      return this.service.isLoggedIn();
    } else {
      return true;
    }
  }


  logout() {
    this.orderFlowService.clearOrder();
    localStorage.removeItem('uid');
    this.events.publish('imageUrl', 'assets/img/profile.jpg');
    this.router.navigate(['/login']);
  }

  inviteOthers() {
    // this.socialSharing.share('Share restaurant App', null, null, 'https://ionicfirebaseapp.com/#/');
  }

}
